<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserActivityLogs extends Migration
{
    public function up()
    {
        Schema::create('user_activity_logs', function (Blueprint $table) {
            $table->increments('id');

            $table->string('event_name')->index();

            $table->integer('target_id')->unsigned();
            $table->foreign('target_id')->references('id')->on('users')->onDelete('cascade');

            $table->integer('triggered_by_id')->unsigned();
            $table->foreign('triggered_by_id')->references('id')->on('users')->onDelete('cascade');

            $table->unique(['event_name', 'target_id', 'triggered_by_id']);

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('user_activity_logs');
    }
}
