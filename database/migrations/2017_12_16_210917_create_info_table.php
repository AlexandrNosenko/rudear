<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInfoTable extends Migration
{
    public function up()
    {
        Schema::create('user_infos', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');;

            // $table->integer('info_id')->unsigned();
            // $table->foreign('info_id')->references('id')->on('infos');

            // $table->string('value')->index()->nullable();
            
            // Kinda optimisation thing
            
            // Preferences
            $table->string('pref_goal')->index()->nullable();
            $table->string('pref_age')->index()->nullable();
            $table->string('pref_gender')->index()->nullable();
            $table->string('pref_country')->index()->nullable();
            
            // Additional info
            $table->string('education')->index()->nullable();
            $table->string('occupation')->index()->nullable();
            $table->string('lang')->index()->nullable();
            $table->string('relations')->index()->nullable();
            $table->boolean('smoker')->index()->nullable()->default(false);
            $table->boolean('drinker')->index()->nullable()->default(false);
            $table->string('children')->index()->nullable();
            $table->string('zodiac')->index()->nullable();
            $table->string('slogan')->index()->nullable();
            
            $table->timestamps();
        });
    }

   public function down()
    {
        Schema::dropIfExists('user_infos');
    }
}
