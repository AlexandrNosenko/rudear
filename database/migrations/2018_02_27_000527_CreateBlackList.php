<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlackList extends Migration
{
    public function up()
    {
        Schema::create('black_lists', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('banned_by_id')->unsigned();
            $table->foreign('banned_by_id')->references('id')->on('users')->onDelete('cascade');

            $table->integer('target_id')->unsigned();
            $table->foreign('target_id')->references('id')->on('users')->onDelete('cascade');

            $table->unique(['banned_by_id', 'target_id']);

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('black_lists');
    }
}
