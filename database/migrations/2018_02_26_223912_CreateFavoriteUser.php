<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFavoriteUser extends Migration
{
    public function up()
    {
        Schema::create('favorite_users', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('favorited_by_id')->unsigned();
            $table->foreign('favorited_by_id')->references('id')->on('users')->onDelete('cascade');

            $table->integer('target_id')->unsigned();
            $table->foreign('target_id')->references('id')->on('users')->onDelete('cascade');

            $table->unique(['favorited_by_id', 'target_id']);

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('favorite_users');
    }

}
