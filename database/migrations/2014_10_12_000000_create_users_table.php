<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->index();
            $table->string('email')->unique()->nullable()->index();
            $table->string('password')->nullable();

            $table->integer('referrer_id')->unsigned()->nullable();
            $table->foreign('referrer_id')->references('id')->on('users')->onDelete('cascade');;

            //Todod extract providers into separate table
            $table->string('provider')->index()->nullable();
            $table->string('provider_id')->index()->nullable();

            $table->enum('gender', ['male', 'female', 'unset'])->index();
            $table->date('birth_date')->nullable()->index();
            $table->text('bio')->nullable();
            
            $table->decimal('height', 4, 1)->nullable()->index();
            $table->decimal('weight', 4, 1)->nullable()->index();

            $table->string('country')->nullable()->index();
            $table->string('city')->nullable()->index();

            $table->enum('status', ['normal', 'vip'])->index()->default('normal');

            $table->rememberToken();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('users');
    }
}
