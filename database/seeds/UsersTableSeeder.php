<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker\Factory::create();
        for($i=0;$i<10;$i++){
          User::create([
            'name' => $faker->name,
            'email' => $faker->email,
            'birth_date' => new \DateTime('19'.$faker->numberBetween(74, 99).'-03-01'),
            'password' => bcrypt('secret'),
            'status' => ($faker->numberBetween(1, 2) % 2 == 0) ? 'vip' : 'normal',
            'gender' => ($faker->numberBetween(1, 2) % 2 == 0) ? 'male' : 'female',
            'country' => ($faker->numberBetween(1, 2) % 2 == 0) ? 'PL' : 'UA',
          ]);
        }
    }
}
