<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Validator;

class BlackList extends Model
{
    protected $fillable = [
        'banned_by_id', 'target_id'
    ];

    private $rules = array(
        'banned_by_id' => 'required|unique_with: black_lists, :target_id'
    );

    public function validate($data)
    {
        $v = Validator::make($data, $this->rules);
        if( $v->passes() )
            return true;
        else{
            $this->errors = $v->errors();
            return false;
        }
    }

    public function target(){
        return $this->belongsTo('App\User', 'target_id')->first();
    }

    public function bannedBy(){
        return $this->belongsTo('App\User', 'banned_by_id')->first();
    }
}
