<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Pusher\Pusher;

use App\Chat;
use App\Message;
use App\Events\MessagePosted;


use Sendpulse\RestApi\ApiClient;
use Sendpulse\RestApi\Storage\FileStorage;

class MessagesController extends Controller
{
    public function indexAjax(Request $request){
        $chat = Chat::find($request->input('chat')['id']);

        return \Response::view('chats/_messages', compact('chat'), 200);
    }

    public function create(Request $request)
    {
        $messageData = $request->input('message')['message'];

        $chat = Chat::findOrCreate($messageData);

        $messageData['chat_id'] = $chat->id;
        // TODO add check if user can write to receiver
        $message = Message::create($messageData);

        if( $message->exists )
          event(new MessagePosted($message));

        return response()->json($message);
    }
}
