<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BlackList;

class BlackListController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request){
        $blackListUsers = \Auth::user()->blackListUsers();
// dd($favoriteUsers);
        return view('favorites/index', compact('blackListUsers'));
    }

    public function create(Request $request)
    {
        $data = [
            'banned_by_id' => \Auth::user()->id,
            'target_id' =>$request->input('target_id')
        ];

        $bu = new BlackList($data);

        if( $bu->validate($data) && $bu->save() ){
            $request->session()->flash('success', 'Successfully added to favorites!');
        }else{
            $request->session()->flash('info', 'Already added!');
        }

        return back();
    }
}
