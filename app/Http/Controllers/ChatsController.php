<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Chat;
use JavaScript;

class ChatsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = \Auth::user();

        $chats = Chat::with($user->id);

        $translations = [
            'chatMessage' => 'Chat Message',
            'searchContact' => __('Search Contact'),
            'typeYourMessage' => __('Type your message'),
            'send' => 'Send',
            'offline' => __('Offline'),
            'online' => __('Online'),
        ];

        JavaScript::put([
            'currentUser' => $user,
            'translations' => $translations
        ]);

        return view('chats/index', compact('chats'));
    }

    public function indexJson()
    {
        $user = \Auth::user();

        $chats = Chat::with($user->id);


        return response()->json($chats);
    }

    public function create(Request $request)
    {
        $chatData = [
          'receiver_id' => $request->input('chat')['receiver_id'],
          'sender_id' => \Auth::user()->id
        ];

        $chat = Chat::findOrCreate($chatData);

        if( $chat->exists ){
            return redirect('/chats');
        }else{
            return back();
        }
    }
}
