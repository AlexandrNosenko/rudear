<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;

class HomeController extends Controller
{
    
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function index(Request $request)
    {
        // \App::setLocale('ru');
        $filters = $this->prepareFilters($request);

        $latestUsers = User::latest()->limit(10)->get();
        $topUsers = $latestUsers;

        $today = date('Y-m-d' . ' 00:00:00', time());

        $usersCount = DB::table('users')->count();
        $usersOnlineCount = 0;

        $manTodayCount = DB::table('users')
            ->where('gender', 'male')
            ->where('created_at', '>=', $today)
            ->count();

        $womanTodayCount = DB::table('users')
            ->where('gender', 'female')
            ->where('created_at', '>=', $today)
            ->count();

        $funFacts = compact(['usersCount', 'usersOnlineCount', 'manTodayCount', 'womanTodayCount']);

        return view('home/index',compact('latestUsers', 'topUsers', 'filters', 'funFacts'));
    }
}
