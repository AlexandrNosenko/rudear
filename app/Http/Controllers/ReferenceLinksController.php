<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Cookie\CookieJar;
use Cookie;

class ReferenceLinksController extends Controller
{
    public function new(Request $request)
    {
        $user = \Auth::user();
        return view('reference-links/new', compact('user'));
    }

    public function useLink(Request $request, $token){
        // http://rudear.local/r/2a2d523ed2ae0ff12cf589866030b8e6
        // return response('Hello World')->cookie($cookie);
        // return dd( cookie('ru_dear_referrer') );

        if($token){
            Cookie::queue(Cookie::make('ru_dear_referrer', $token, 0));
        }

        return redirect('register');
    }
}
