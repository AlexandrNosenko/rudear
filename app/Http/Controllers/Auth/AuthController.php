<?php

namespace App\Http\Controllers\Auth;

use Auth;
use Socialite;
use Cookie;

use App\User;
use App\Photo;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;

class AuthController extends Controller
{
  /**
  * Redirect the user to the OAuth Provider.
  *
  * @return Response
  */

  private function getProviderFields($provider){
    $fields = ['name', 'email'];
// TODO fetch counrty from api
    switch ($provider) {
      case 'facebook':
        $fields = array_merge($fields, ['birthday, gender']);
        break;
      
      case 'vkontakte':
        $fields = array_merge($fields, ['bdate, photo_max_orig, sex']);
        break;
    }
    return $fields;
  }
  private function getProviderScopes($provider){
    $scopes = ['email', 'user_birthday'];

    // switch ($provider) {
    //   case 'facebook':
    //     $scopes = array_merge($scopes, ['birthday']);
    //     break;
      
    //   case 'vkontakte':
    //     $scopes = array_merge($scopes, ['bdate']);
    //     break;
    // }
    return $scopes;
  }
  public function redirectToProvider($provider)
  {
   $fields = $this->getProviderFields($provider);
   $scopes = $this->getProviderScopes($provider);

    return Socialite::driver($provider)->fields($fields)->scopes($scopes)->redirect();
  }
  
  private function adaptData( $provider, $userResponse)
  {
    $user = $userResponse->user;

    switch ($provider) {
      case 'facebook':
        $user['avatar'] = $userResponse->avatar_original;
        $user['birth_date'] = $user['birthday'];
        unset($user['birthday']);
        $user['provider_id'] = $user['id'];
        unset($user['id']);
        break;
      
      case 'vkontakte':
        $user['provider_id'] = $user['uid'];
        unset($user['uid']);

        $user['name'] = $user['first_name'] . " " . $user['last_name'];
        unset($user['first_name']);
        unset($user['last_name']);

        $user['birth_date'] = $user['bdate'];
        unset($user['bdate']);

        $user['avatar'] = $user['photo_max_orig'];
        unset($user['photo_max_orig']);

        $user['gender'] = $user['sex'] === 2 ? 'male' : ($user['sex'] === 1 ? 'female' : 'unset');
        unset($user['sex']);

        break;
    }

    $user['birth_date'] = date('Y-m-d', strtotime($user['birth_date']));

    return $user;
  }
  /**
   * Obtain the user information from provider.  Check if the user already exists in our
   * database by looking up their provider_id in the database.
   * If the user exists, log them in. Otherwise, create a new user then log them in. After that 
   * redirect them to the authenticated users homepage.
   *
   * @return Response
   */
  public function handleProviderCallback($provider, Request $request)
  {

    $fields = $this->getProviderFields($provider);

    try {
      $userResponse = Socialite::driver($provider)->fields($fields)->user();
      $user = $this->adaptData($provider, $userResponse);
    } catch (\Laravel\Socialite\Two\InvalidStateException $e) {
      return redirect('/register');
    }

    $user['provider'] = $provider;

    $authUser = User::findOrInitializeUser($user);
    
    $errors = isset($authUser->errors) ? $authUser->errors->all() : [];
    
    if( empty($errors) ){
      $authUser->assignReferrer(Cookie::get('ru_dear_referrer'));

      Auth::login($authUser, true);
      $request->session()->flash('success', 'Welcome to RuDear!');
      return redirect('/profiles');
    }else{
      // $request->session()->flash('danger', 'Error occured' + join($errors, ", "));
      return redirect('/register')->withErrors($errors);
    }
  }
}