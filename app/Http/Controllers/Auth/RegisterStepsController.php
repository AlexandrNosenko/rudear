<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Photo;
use App\Info;
use Auth;
use Cookie;

// use Illuminate\Auth\GenericUser;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Requests\HomepageRegisterRequest;

class RegisterStepsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['homepage']]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    
    protected function photos()
    {
        $photos = \Auth::user()->photos()->orderBy('type')->get();
        return view('auth/steps/photos', compact('photos') );
    }

    protected function info()
    {
        $user = \Auth::user();
        // $infosFileds = Info::where('type', 'info')->orderBy('order')->get();
        return view('auth/steps/info', compact('user') );
    }

    protected function preferences()
    {
        $user = \Auth::user();
        // $prefFileds = UserInfo::where('type', 'pref')->orderBy('order')->get();

        return view('auth/steps/preferences', compact('user') );
    }

    protected function homepage(HomepageRegisterRequest $request)
    {
        $user = new User;
        $userData = $request->only($user->getFillableWithPrefix("user"));
        $user->fill($userData['user']);
        $user->password = md5(time());
        $user->email = $request->input('email');
        
        if( $user->save() ){
            $user->assignReferrer(Cookie::get('ru_dear_referrer'));

            $user_infos = $user->setDefaultPreferences();
            $user_infos->pref_gender = $request->input('user_infos')['pref_gender'];
            $user_infos->save();
            
            Auth::login($user, true);
            $request->session()->flash('success', 'Welcome to RuDear!');
            return redirect('/profiles');
        }
        return redirect('/');
    }
}
