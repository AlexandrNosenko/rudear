<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\FavoriteUser;
use App\Loggers\UserActivityLogger;

class FavoriteUsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request){
        $favoriteUsers = \Auth::user()->favorites();
        return view('favorites/index', compact('favoriteUsers'));
    }

    public function create(Request $request)
    {
        $data = [
            'favorited_by_id' => \Auth::user()->id,
            'target_id' => $request->input('target_id')
        ];

        $fu = new FavoriteUser($data);

        if( $fu->validate($data) && $fu->save() ){
            $request->session()->flash('success', 'Successfully added to favorites!');
        }else{
            $request->session()->flash('info', 'Already added!');
        }

        UserActivityLogger::register('profile_like', [
            'liked_user' => User::find($request->input('target_id')),
            'liking_user' => \Auth::user()
        ]);

        return back();
    }
}
