<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\TrialVipPromotion;

class VipStatusesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function trial(Request $request){
        $user = \Auth::user();

        $promotion = TrialVipPromotion::grantTo($user);

        $errors = isset($promotion->errors) ? $promotion->errors->all() : [];

        if( empty($errors) )
          $request->session()->flash('success', 'Vip activated!');

        return redirect()->route('register-info')->withErrors($errors);
    }
}
