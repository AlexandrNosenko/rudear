<?php

namespace App\Http\Controllers;

use App\User;
use App\Photo;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class PhotosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    protected function create(Request $request)
    {
        if( $request->hasFile('photos') )
        {
            $image = $request->file('photos');

            $photo = Photo::uploadAndSave(['file' => $image, 'user' => \Auth::user()]);

            if ( $photo->save() )
              $request->session()->flash('success', 'Image saved!');
        }
        
        return redirect('/register/photos');

    }

    protected function delete($id, Request $request)
    {
        $photo = \Auth::user()->photos()->where('id', $id)->first();
        if ( Photo::destroy($photo->id) )
          $request->session()->flash('success', 'Image deleted!');

        return redirect('/register/photos');
    }

    protected function update($id, Request $request)
    {
        $photo = \Auth::user()->photos()->where('id', $id)->first();
        $photo->type = $request->input('type');

        if ( $photo->save() )
          $request->session()->flash('success', 'Image saved!');

        return redirect('/register/photos');
    }

}
