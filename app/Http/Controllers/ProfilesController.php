<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Loggers\UserActivityLogger;
use App\User;
use App\Http\Requests\UpdateUserPreferences;
use App\Http\Requests\UpdateUserInfo;

class ProfilesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index']]);
    }

    public function index(Request $request)
    {

        $filters = $this->prepareFilters($request);

        $vipUsers = User::filter(array_merge($filters, ['pref_status' => 'vip']))->paginate(4);
        $latestUsers = User::filter(array_merge($filters))->paginate(4);
        $allUsers = User::filter(array_merge($filters))->paginate(4);

        return view('profiles/index',compact('latestUsers', 'vipUsers', 'allUsers', 'filters'));
    }

    public function indexAjax(Request $request)
    {
        $filters = $this->prepareFilters($request);

        $users = User::filter($filters)->paginate(4);

        $elemClass = 'profile-text';

        return \Response::view('profiles/_users_list', compact('users', 'elemClass'), 200);
    }


    public function edit(){
        $photos = \Auth::user()->photos()->orderBy('type')->get();
        return view('auth/steps/photos', compact('photos') );
    }

    public function show($id){
        $user = User::find($id);

        $vipUsers = User::getMainVip()->get();
        $recommendedUsers = \Auth::user()->getPossibleMatches()->limit(4)->get();

        $TODO_REFACTOR_send_msg_permission = \Auth::user()->hasPermissionTo('SendMessage', ['user' => $user]);
// dd($user, \Auth::user());
        UserActivityLogger::register('page_visit', ['visited_user' => $user, 'visiting_user' => \Auth::user()]);

        return view('profiles/show', compact('user', 'vipUsers', 'recommendedUsers', 'TODO_REFACTOR_send_msg_permission'));
    }

    public function updateInfo(UpdateUserInfo $request){
        // TODO use validations
        $user = \Auth::user();

        $userData = $request->only($user->getFillableWithPrefix("user"));
// dd($userData);
        $user->fill($userData['user']);

        // TODO remove when in production
        $user->status = $request->input('status');

        # TODO Strong params
        $userInfo = $user->infos();
        $userInfoData = $request->only($userInfo->getFillableWithPrefix("user_infos"));

        $userInfo->fill($userInfoData['user_infos']);

        if( !isset($userInfo->zodiac) && isset($user->birth_date) )
            $userInfo->setZodiac($user->birth_date);

        if ( $user->save() && $userInfo->save() )
          $request->session()->flash('success', 'Info saved!');

        return redirect('/register/info');
    }

    public function updatePref(UpdateUserPreferences $request){
        $user = \Auth::user();

        # TODO Strong params
        $userInfo = $user->infos();
        $userInfoData = $request->only($userInfo->getFillableWithPrefix("user_infos"));

        if( is_array($userInfoData['user_infos']['pref_goal']) )
            $userInfoData['user_infos']['pref_goal'] = implode(", ", $userInfoData['user_infos']['pref_goal']);

        $userInfo->fill($userInfoData['user_infos']);
        if ( $userInfo->save() )
          $request->session()->flash('success', 'Info saved!');

        return redirect('/register/preferences');
    }
}
