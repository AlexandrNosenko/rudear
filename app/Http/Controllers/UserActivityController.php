<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserActivityController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request){
        $likes = \Auth::user()->likes()->get();
        return view('likes/index', compact('likes'));
    }

}
