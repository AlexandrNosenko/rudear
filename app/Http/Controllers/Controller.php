<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function prepareFilters(Request $request){
      // TODO add strong params
      // TODO refactor default filter dandling

      $filters = $request->input('filters');

      if(!isset($filters)) $filters = [];
      // if (!isset($filters['pref_status'])) $filters['pref_status'] = 'normal';
      $user = \Auth::user();

      if (!isset($filters['pref_age_from']))
          $filters['pref_age_from'] = isset($user) ? $user->infos()->prefAgeMin() : '18';

      if (!isset($filters['pref_age_to']))
          $filters['pref_age_to'] = isset($user) ? $user->infos()->prefAgeMax() : '27';

      if (!isset($filters['pref_gender']))
          $filters['pref_gender'] = isset($user) ? $user->infos()->pref_gender : 'female';

      if (!isset($filters['pref_country']))
          $filters['pref_country'] = isset($user) && isset($user->country) ? $user->country : 'PL';
      // }else{
      //     $filters = [
      //         'pref_age_from' => '18',
      //         'pref_age_to' => '27',
      //         'pref_gender' => 'female',
      //         'pref_country' => 'pl',

      //     ];
      // }
      // dd($filters);
      return $filters;
  }
}
