<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Payment;

class PaymentsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $paymentData = Payment::paypalPayment(['user_id' => \Auth::user()->id]);
        $redirectUrl = $paymentData['redirectUrl'];
        $payment = $paymentData['payment'];

        return redirect( $redirectUrl );
        // dd(Paypal::getAll(array('count' => 1, 'start_index' => 0), $this->_apiContext));
    }

    public function paypalSuccess(Request $request)
    {
      $payment = Payment::executePaypalPayment([
        'id' => $request->get('paymentId'),
        'token' => $request->get('token'),
        'payer_id' => $request->get('PayerID')
      ]);

      $request->session()->flash('success', 'Congatulations, you\'ve become a VIP member!');
      return redirect()->route('root');
    }

    public function paypalCancel(Request $request)
    {
        $request->session()->flash('danger', 'Payment Error occured!');
        return redirect()->route('root');
    }
}
