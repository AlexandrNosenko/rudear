<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HomepageRegisterRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            'user.gender' => 'required',
            'user.name' => 'required',
            'email' => 'required|unique:users',
            'user_infos.pref_gender' => 'required',
            // 'user_infos.pref_goal' => 'required',
            // 'user_infos.pref_gender' => 'required',
            // 'user_infos.pref_age' => 'required'
        ];
    }
}