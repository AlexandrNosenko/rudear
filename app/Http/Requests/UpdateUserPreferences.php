<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserPreferences extends FormRequest
{
    public function authorize()
    {
        $user = \Auth::user();
        return isset($user);
    }

    public function rules()
    {
        return [
            'user_infos.pref_goal' => 'required',
            'user_infos.pref_gender' => 'required',
            'user_infos.pref_age' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'user_infos.pref_goal.required' => 'Your goal is missing',
            'user_infos.pref_gender.required'  => 'Prefered gender is missing',
            'user_infos.pref_age.required'  => 'Preffered age is missing',
        ];
    }
}
