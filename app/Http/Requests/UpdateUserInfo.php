<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserInfo extends FormRequest
{
    public function authorize()
    {
        $user = \Auth::user();
        return isset($user);
    }

    public function rules()
    {
        return [
            'user.gender' => 'required',
            'user.birth_date' => 'required',
            // 'user_infos.pref_goal' => 'required',
            // 'user_infos.pref_gender' => 'required',
            // 'user_infos.pref_age' => 'required'
        ];
    }
}
