<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class SetLocale
{
    protected $except = [];

    public function handle($request, Closure $next)
    {
        $languages = $request->server('HTTP_ACCEPT_LANGUAGE');
        $mainLang = substr($languages, 0, 2);

        // Set the language
        if(in_array($mainLang, config('app.locales')))
        {
            Session::put('the_cupidone.language_locale', $request->segment(1));
        }

        // Check if the session has the language
        if(!Session::has('the_cupidone.language_locale'))
            Session::put('the_cupidone.language_locale', config('app.fallback_locale'));

        app()->setLocale(Session::get('the_cupidone.language_locale'));

        return $next($request);
    }
}
