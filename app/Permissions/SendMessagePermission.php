<?php

namespace App\Permissions;

class SendMessagePermission {

    function __construct($data) {
        $this->data = $data;
        $this->errors = [];
        $this->rules = [
            'vip' => $this->data['target']->vip(),
            'blacklist' => !$this->data['target']->isBannedBy($data['user'])
        ];
    }

    public function resolve(){
        // TODO implement logic to filter if receiver wants to receive message from target
        // dd($this->data);
        foreach ($this->rules as $key => $rule) {
            if(!$rule) {
                array_push($this->errors, $key);
            }
        }

        if( count($this->errors) > 0 ) return false;
        return  true;
    }
}
