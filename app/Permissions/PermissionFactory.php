<?php
namespace App\Permissions;

class PermissionFactory {
  public static function build($permissionName, $data){
    $permissionClass = 'App\Permissions\\'.$permissionName.'Permission';

    return new $permissionClass($data);
  }
}
