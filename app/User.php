<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

use Validator;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'name', 'email', 'password', 'provider', 'provider_id', 'gender', 'birth_date', 'height', 'weight', 'country', 'city', 'bio'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    private $rules = array(
        'email' => 'required|unique:users',
        // 'status' => 'required'
    );

    protected $appends = ['avatar', 'is_online'];

    private $infosCache = [];

    public function validate($data)
    {
        $v = Validator::make($data, $this->rules);
        if( $v->passes() )
            return true;
        else{
            $this->errors = $v->errors();
            return false;
        }
    }


    /* ------------------------- */
    /* ------Object methods----- */
    /* ------------------------- */

    public function getFillableWithPrefix($prefix = "")
    {
        if(strlen($prefix) > 0) $prefix = $prefix.".";

        $res = [];
        foreach ($this->fillable as $field) {
            array_push($res, $prefix.$field);
        }
        return $res;
    }

    public function photos()
    {
        return $this->hasMany('App\Photo');
    }

    public function likes() {
        return $this->hasMany('App\UserActivityLog', 'target_id');
    }

    public function favorites()
    {
        return $this->hasManyThrough('App\User', 'App\FavoriteUser', 'favorited_by_id', 'id', 'id', 'favorited_by_id')->get();
    }

    public function blackList()
    {
        return $this->hasManyThrough('App\User', 'App\BlackList', 'banned_by_id', 'id', 'id', 'banned_by_id');
    }

    public function payments()
    {
        return $this->hasMany('App\Payment');
    }

    public function referrer()
    {
        return $this->hasOne('App\User', 'referrer_id')->get()->first();
    }

    public function getPhotos()
    {
        return $this->photos()->get();
    }

    public function infos()
    {
        $infos = $this->hasOne('App\UserInfo')->get()->first();
        if(empty($infos)){
            //To avoid user_id being reasigned

            $infos = new UserInfo;
            $infos->user_id = $this->id;
            $infos->save();
        }
       return $infos;
    }

    public function referenceLink()
    {
        $link = $this->hasOne('App\ReferenceLink')->get()->first();

        if( !isset($link) ){
            $link = ReferenceLink::createFor($this);
        }

        return $link;

    }

    public function assignReferrer($referrer_link){
        $ref_link = ReferenceLink::where('value', $referrer_link)->get()->first();
        if( $ref_link ) $this->referrer_id = $ref_link->owner()->id;
        $this->save();
    }

    public function getAvatarAttribute()
    {
        return $this->mainPhoto();
    }

    public function getIsOnlineAttribute()
    {
        return $this->isOnline();
    }


    public function mainPhoto()
    {
        $mainPhoto = $this->photos()->where('type', 'main')->first();

        if($mainPhoto){
            return $mainPhoto->url();
        }else{
            return "/images/profile/01.jpg";
        }
    }

    public function age(){
        $years = date('Y', strtotime($this->birth_date));
        return date('Y') - $years - 1;
    }

    public function livesIn()
    {
        return implode(", ", [$this->city, $this->country]);
    }

    public function vip(){
        return $this->status == 'vip';
    }

    public function isOnline()
    {
        return \Cache::has('user-is-online-' . $this->id);
    }

    public function setDefaultPreferences(){
        $pref_gender = $this->gender == 'male' ? 'female' : 'male';

        $user_infos = $this->infos();
        $user_infos->pref_gender = $pref_gender;
        $user_infos->pref_goal = 'friends';
        $user_infos->pref_age = '18-30';
        $user_infos->pref_country = isset($this->country) ? $this->country : 'PL';

        $user_infos->save();
        return $user_infos;
    }

    public function setDefaultInfo($data){
        $user_infos = $this->infos();
        if( isset($data['country']) )
            $this->country = $data['country'];

        $user_infos->setZodiac($this->birth_date);
    }

    public function getPossibleMatches(){
        $user_infos = $this->infos();
        // $filters = [
        //     'pref_country' => $user_infos['pref_country'],
        //     'pref_gender' =>
        // ];
        $matches = User::filter($user_infos);

        return $matches;
    }

    public function hasPermissionTo($permissionName, $data){
        $data = array_merge($data, ['target' => $this]);
        $permissionInst = Permissions\PermissionFactory::build($permissionName, $data);
        $permissionInst->resolve();
        if( count($permissionInst->errors) > 0 )
            return ['ok' => false, 'errors' => $permissionInst->errors];
        return ['ok' => true];
    }

    public function isBannedBy($user){
        $matchedUser = $user->blackList()->where('black_lists.target_id', $user->id)->first();

        if( isset($matchedUser) ) return true;

        return false;
    }
    /* ------Object methods----- */
    /* ------------------------- */


    /* ------------------------- */
    /* ------Class methods----- */
    /* ------------------------- */

    public static function filter($filters){
        $res = User::latest()->limit(20);

        foreach ($filters as $key => $value) {
            switch ($key) {
                case 'pref_country':
                    $res = $res->where('country', $value);
                    break;
                case 'pref_gender':
                    if( $value != 'both' )
                        $res = $res->where('gender', $value);
                    break;
                case 'pref_status':
                    $res = $res->where('status', $value);
                    break;
                case 'pref_age_from':
                    $time = strtotime("-".$value." year", time());
                    $date = date("Y-m-d", $time);

                    $res = $res->where('birth_date', '<=', $date);
                    break;
                case 'pref_age_to':
                    $time = strtotime("-".$value." year", time());
                    $date = date("Y-m-d", $time);
                    // dd($filters, $value, $date);
                    $res = $res->where('birth_date', '>=', $date);
                    break;
            }
        }
        // dd($filters, $res->toSql());
        return $res;
    }

    public static function getMainVip(){
        return User::where('status', 'vip')->latest()->limit(6);
    }

    public static function findOrInitializeUser($userData)
    {
        $user = User::where('provider_id', $userData['provider_id'])->orWhere('email', $userData['email'])->first();
        if ($user) return $user;

        $user = new User;
        $user->fill($userData);

        if( $user->validate($userData) && $user->save() )
        {
            Photo::create([
              'user_id' => $user->id,
              'title' => substr($userData['avatar'], 33, 43),
              'type' => 'main',
              'remote_url' => $userData['avatar']
            ]);
            $user->setDefaultPreferences();
            $user->setDefaultInfo($userData);
        }

        return $user;
    }

    public static function updateVipStatuses(){
        // $users = User::with('payments')->get();
        $payed_users = Payment::where('valid_until', '>=', date("Y-m-d H:i"))
            ->where('type', 'vip_access')
            ->pluck('user_id');

        User::whereNotIn('id', $payed_users)->update(['status' => 'normal']);
    }
    /* ------Class methods----- */
    /* ------------------------- */

}
