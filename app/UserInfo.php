<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserInfo extends Model
{
    protected $casts = [
      'drinker' => 'boolean',
      'smoker' => 'boolean'
    ];
    protected $fillable = [
        "education", "occupation", "lang", "relations", "slogan", "children", "drinker", "smoker", "pref_goal", "pref_age", "pref_gender"
    ];
    
    public function getFillableWithPrefix($prefix = "")
    {
        if(strlen($prefix) > 0) $prefix = $prefix.".";

        $res = [];
        foreach ($this->fillable as $field) {
            array_push($res, $prefix.$field);
        }
        return $res;
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function info()
    {
        return $this->belongsTo('App\Info');
    }

    public function haveGoal($goal){
        return strpos($this->pref_goal, $goal) !== false;
    }

    public function prefAgeMax(){
        $ages = preg_split("/-/", $this->pref_age);
        if( count($ages) > 1 )
          return $ages[1];
        else
          return 35;
    }

    public function prefAgeMin(){
        $ages = preg_split("/-/", $this->pref_age);
        if( count($ages) > 1 )
          return $ages[0];
        else
          return 18;
    }

    public function setZodiac($date){
        if( $this->validDate($date) ){
            $this->zodiac = $this->zodiacFrom($date);
            $this->save();
        }
    }

    private function validDate($date){
        $d = \DateTime::createFromFormat('Y-m-d', $date);
        return $d && $d->format('Y-m-d') == $date;
    }

    private function zodiacFrom($birthDate) {
        $zodiac = '';     
        list ($year, $month, $day) = explode ('-', $birthDate); 
             
        if ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiac = "Aries"; } 
        elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiac = "Taurus"; } 
        elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiac = "Gemini"; } 
        elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiac = "Cancer"; } 
        elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiac = "Leo"; } 
        elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiac = "Virgo"; } 
        elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiac = "Libra"; } 
        elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiac = "Scorpio"; } 
        elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiac = "Sagittarius"; } 
        elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiac = "Capricorn"; } 
        elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiac = "Aquarius"; } 
        elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiac = "Pisces"; } 
     
        return $zodiac; 
    }

}
