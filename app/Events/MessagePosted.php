<?php

namespace App\Events;
use App\Message;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use Sendpulse\RestApi\ApiClient;
use Sendpulse\RestApi\Storage\FileStorage;

class MessagePosted implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $message;
    public $user;

    public function __construct(Message $message)
    {
        $this->message = $message;
        $this->user = $message->sender()->get();
    }

    public function broadcastOn()
    {
        // $SPApiClient = new ApiClient(getenv('SENDPULSE_API_ID'), getenv('SENDPULSE_API_SECRET'), new FileStorage());

        // $task = array(
        //     'title' => __('You have a new message!'),
        //     'body' => 'Click to view more',
        //     'website_id' => getenv('SENDPULSE_SITE_ID'),
        //     'ttl' => 200,
        //     'stretch_time' => 0,
        // );

        // $filters = '{"variable_name":"user_email","operator":"or","conditions":[{"condition":"equal","value":"' . $this->message->receiver()->email.'"}]}';

        // $additionalParams = array(
        //     'link' => 'http://thecupidone.com/chats',
        //     'filter_browsers' => 'Chrome,Safari',
        //     'filter' => $filters,
        // );
        // $SPApiClient->createPushTask($task, $additionalParams);
        // TODO make channels private
        // echo 'chatroom' . $this->message->chat()->id;
        return new Channel('chatroom' . $this->message->chat()->id);
    }
}
