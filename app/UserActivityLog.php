<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Validator;

class UserActivityLog extends Model
{
    protected $fillable = [
        'target_id', 'triggered_by_id', 'event_name'
    ];

    private $rules = array(
        // 'event_name' => 'required',
        'event_name' => 'required|unique_with: user_activity_loggs, :target_id, :triggered_by_id'
    );

    public function validate($data)
    {
        $v = Validator::make($data, $this->rules);
        if( $v->passes() ){
            return true;
        }else{
            $this->errors = $v->errors();
            // dd($this->errors);
            return false;
        }
    }

    public function triggered_by(){
        return $this->belongsTo('App\User', 'triggered_by_id')->first();
    }
}
