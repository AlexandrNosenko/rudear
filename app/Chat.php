<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{

    protected $fillable = [
        'sender_id', 'receiver_id'
    ];

    protected $with = array('messages', 'sender', 'receiver');

    public function Messages(){
      return $this->hasMany('App\Message', 'chat_id')
        ->orderBy('created_at', 'ASC');
    }

    public function Sender(){
        return $this->belongsTo('App\User', 'sender_id');
    }

    public function Receiver(){
        return $this->belongsTo('App\User', 'receiver_id');
    }

    public function decideReceiver($sender_id){
      $receiver_id = $sender_id == $this->sender_id ? $this->receiver_id : $this->sender_id;

      return User::find($receiver_id);
    }

    public static function findOrCreate($data){

      $chat = self::where(function($query) use ($data){
          $query->where('sender_id', $data['sender_id']);
          $query->where('receiver_id', $data['receiver_id']);
      })->orWhere(function($query) use ($data){
          $query->where('receiver_id', $data['sender_id']);
          $query->where('sender_id', $data['receiver_id']);
      })->first();

      if( !isset($chat) )
        $chat = self::create($data);

      return $chat;
    }

    public static function with($userId){
      return self::where('sender_id', $userId)
          ->orWhere('receiver_id', $userId)
          ->get();
    }
}
