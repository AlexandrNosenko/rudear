<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReferenceLink extends Model
{
    protected $fillable = [
        "value"
    ];

    public function owner()
    {
        return $this->belongsTo('App\User', 'user_id')->first();
    }

    public static function createFor(User $user){
        $link = new ReferenceLink;

        $link->value = ReferenceLink::getRandomValue($user);
        $link->user_id = $user->id;
        
        $link->save();

        return $link;
    }

    public static function getRandomValue(User $user){
        return md5(getenv('APP_SECRET_TOKEN') . $user->id);
    }
}
