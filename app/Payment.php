<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Paypal;

class Payment extends Model
{
    private static $_paypalApiContext;

    protected $fillable = ['user_id', 'type', 'status', 'currency', 'amount', 'valid_until', 'source', 'invoice'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    private static function paypalContext(){
        if( !isset(self::$_paypalApiContext) ){
            self::$_paypalApiContext = PayPal::ApiContext(
                config('services.paypal.client_id'),
                config('services.paypal.secret'));

            self::$_paypalApiContext->setConfig(array(
              'mode' => 'sandbox',
              'service.EndPoint' => 'https://api.sandbox.paypal.com',
              'http.ConnectionTimeOut' => 30,
              'log.LogEnabled' => true,
              'log.FileName' => storage_path('logs/paypal.log'),
              'log.LogLevel' => 'FINE'
            ));
        }
        return self::$_paypalApiContext;
    }


    public static function paypalPayment($data) {
        $paypalPayment = self::requestPaypalPayment([
            'currency' => 'EUR',
            'amount' => 3
        ]);

        $amount = $paypalPayment->transactions[0]->amount;

        $payment = Payment::create(array_merge($data, [
            'status' => 'new',
            'type' => 'vip_access',
            'valid_until' => date('Y-m-d H:i', strtotime("+30 days")),
            'currency' => $amount->currency,
            'amount' => $amount->total,
            'source' => 'paypal',
            'invoice' => json_encode([
                'vendor' => 'paypal',
                'paymentId' => $paypalPayment->id,
                'currency' => $amount->currency,
                'amount' => $amount->total
            ])
        ]));

        return [
            'redirectUrl' => $paypalPayment->links[1]->href,
            'payment' => $payment
        ];
    }

    //TODO create vipstatus payment class to calculate valid_until, amount, currency, type, automaticaly
    public static function requestPaypalPayment($data){
        $_apiContext = self::paypalContext();

        $payer = PayPal::Payer();
        $payer->setPaymentMethod('paypal');

        $amount = PayPal:: Amount();
        $amount->setCurrency($data['currency']);
        $amount->setTotal($data['amount']); // This is the simple way,
        // you can alternatively describe everything in the order separately;
        // Reference the PayPal PHP REST SDK for details.

        $transaction = PayPal::Transaction();
        $transaction->setAmount($amount);
        $transaction->setDescription('What are you selling?');

        $redirectUrls = PayPal:: RedirectUrls();
        $redirectUrls->setReturnUrl(action('PaymentsController@paypalSuccess'));
        $redirectUrls->setCancelUrl(action('PaymentsController@paypalCancel'));

        $paypalPayment = PayPal::Payment();
        $paypalPayment->setIntent('sale');
        $paypalPayment->setPayer($payer);
        $paypalPayment->setRedirectUrls($redirectUrls);
        $paypalPayment->setTransactions(array($transaction));

        return $paypalPayment->create($_apiContext);
    }

    public static function executePaypalPayment($data){
        $_apiContext = self::paypalContext();

        $paypalPayment = PayPal::getById($data['id'], $_apiContext);

        $paymentExecution = PayPal::PaymentExecution();
        $paymentExecution->setPayerId($data['payer_id']);

        $executePayment = $paypalPayment->execute($paymentExecution, $_apiContext);

        $payment = self::where('invoice->paymentId', $data['id'])->get()->first();
        $paymentInvoice = json_decode($payment->invoice);
        $paymentInvoice->token = $data['token'];
        $paymentInvoice->payer_id = $data['payer_id'];

        $payment->invoice = json_encode($paymentInvoice);
        $payment->status = 'paid';
        $payment->save();

        $user = $payment->user;
        $user->status = 'vip';
        $user->save();

        return $payment;
    }
}
