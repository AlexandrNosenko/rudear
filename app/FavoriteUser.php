<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Validator;

class FavoriteUser extends Model
{
    protected $fillable = [
        'favorited_by_id', 'target_id'
    ];

    private $rules = array(
        'favorited_by_id' => 'required|unique_with: favorite_users, :target_id'
    );

    public function validate($data)
    {
        $v = Validator::make($data, $this->rules);
        if( $v->passes() )
            return true;
        else{
            $this->errors = $v->errors();
            return false;
        }
    }

    public function target(){
        return $this->belongsTo('App\User', 'target_id')->first();
    }

    public function favoritedBy(){
        return $this->belongsTo('App\User', 'favorited_by_id')->first();
    }

}
