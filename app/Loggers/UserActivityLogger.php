<?php

namespace App\Loggers;
use App\UserActivityLog;

class UserActivityLogger {

    public static function register($eventName, $data) {
        if( $eventName == 'page_visit' ){
            $user_activity = new UserActivityLog([
                'event_name' => $eventName,
                'triggered_by_id' => $data['visiting_user']->id,
                'target_id' => $data['visited_user']->id
            ]);
            $user_activity->validate($data);

            return $user_activity->validate($data) && $user_activity->save();
        }else if( $eventName == 'profile_like' ){

            $user_activity = new UserActivityLog([
                'event_name' => $eventName,
                'triggered_by_id' => $data['liking_user']->id,
                'target_id' => $data['liked_user']->id
            ]);

            return $user_activity->validate($data) && $user_activity->save();
        }
    }
}
