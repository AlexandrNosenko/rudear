<?php
namespace App;

use Illuminate\Support\MessageBag;

class TrialVipPromotion extends Promotion
{
    private $rules = array(
        'user_id' => 'required'
    );
    static $promotion;
    static $user;

    /* ------------------------- */
    /* ------Class methods----- */
    /* ------------------------- */
    
    public static function grantTo(User $user){
        self::$user = $user;

        $data = [
            'user_id' => $user->id,
            'type' => 'vip_trial',
            'expires_at' => date('Y-m-d H:i', strtotime("+3 days"))
        ];

        $promotion = (new Promotion)->newInstance($data);
        self::$promotion = $promotion;

        if( $promotion->validate($data) && self::valid($user) && $promotion->save()){
            self::apply();
        }

        return $promotion;
    }
    
    public static function apply(){
        
        $payment = Payment::create([
            'user_id' => self::$user->id,
            'amount' => 0,
            'currency' => 'zt',
            'type' => 'vip_access',
            'status' => 'success',
            'valid_until' => self::$promotion->expires_at,
            'source' => 'system'
        ]);

        if( $payment->exists ){
            self::$user->status = 'vip';
            self::$user->save();
        }

    }

    public static function valid( User $user ){
        $promotions = Promotion::where('user_id', $user->id)->where('type', 'vip_trial')->get();

        if( !$promotions->isEmpty() ){
            $errors = new MessageBag([]);
            $errors->add('user_id', 'Already granted!');
            
            self::$promotion->errors = $errors;
            return false;
        }
        return true;
    }
    

    /* ------Class methods----- */
    /* ------------------------- */
}
