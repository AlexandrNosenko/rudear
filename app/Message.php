<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = [
        'text', 'receiver_id', 'sender_id', 'chat_id'
    ];
    protected $with = array('sender', 'receiver');

    public function isOwnedBy($user_id)
    {
        return $this->sender_id == $user_id;
    }


    public function Sender(){
        return $this->belongsTo('App\User', 'sender_id');
    }

    public function Receiver(){
        return $this->belongsTo('App\User', 'receiver_id');
    }

    public function chat(){
        return $this->belongsTo('App\Chat', 'chat_id')->first();
    }
}
