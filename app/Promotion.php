<?php

namespace App;
use Validator;

use Illuminate\Database\Eloquent\Model;

class Promotion extends Model
{
    protected $fillable = [
        'user_id', 'type', 'expires_at'
    ];

    private $rules = array(
        'user_id' => 'required',
        'type' => 'required',
        'expires_at' => 'date_format:Y-m-d H:i'
    );
    
    public function validate($data)
    {
        $v = Validator::make($data, $this->rules);
        if( $v->passes() )
            return true;
        else{
            $this->errors = $v->errors();
            return false;
        }
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id')->first();
    }
}
