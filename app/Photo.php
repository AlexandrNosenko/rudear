<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
  private static $imageSizes = [
    'portrait' => [270, 415],
    'wide' => [195, 168]
  ];

  private function assertOneMainPhoto(){
      Photo::where('user_id', $this->user_id)->where('id', '!=', $this->id)->update(['type' => 'normal']);

      if( $this->type != 'main' || !$this->exists ){
        $firstPhoto = Photo::where('user_id', $this->user_id)->orderBy('created_at')->first();

        if($firstPhoto){
          $firstPhoto->type = 'main';
          $firstPhoto->save();
        }
      }
  }

  public static function boot()
  {
      parent::boot();

      // Prevent multiple main photos
      self::created(function($model){ $model->assertOneMainPhoto(); });
      self::deleted(function($model){ $model->assertOneMainPhoto(); });
      self::updated(function($model){ $model->assertOneMainPhoto(); });

  }

  protected $fillable = ['title', 'user_id', 'type', 'remote_url'];

  public function user()
  {
      return $this->belongsTo('App\User');
  }

  public function url(){
    if( strpos($this->remote_url, 'uploads') !== false )
    {
      return "/" . $this->remote_url;
    }
    else
    {
      return $this->remote_url;
    }
  }

  public function dimensions()
  {
    $path = strpos($this->remote_url, 'uploads') ? "../" . $this->remote_url : $this->remote_url;

    $info = getimagesize ($path);
    return  $info[0]."x".$info[1];
  }

  public static function uploadAndSave($data){
    $folder_path = 'uploads/user_images/' . $data['user']->id . '/';

    if ( !file_exists($folder_path) ) mkdir( $folder_path, 0777, true );

    $originalName = time();
    $extension = $data['file']->getClientOriginalExtension();

    // TODO user decided cropping dimensions;
    // foreach (self::$imageSizes as $sizeName => $imageSize) {
    //   $resizedFilename  = $originalName . '@' . $sizeName . '.' . $extension;
    //   $resizedPath = public_path($folder_path . $resizedFilename);
    //   \Image::make($data['file']->getRealPath())->resize($imageSize[0], $imageSize[1])->save($resizedPath);
    // }

    $filename = $originalName . "." . $extension;
    $data['file']->move($folder_path, $filename);

    $photo = Photo::create([
      'user_id' => $data['user']->id,
      'title' => $filename,
      'type' => 'normal',
      'remote_url' => $folder_path . $filename
    ]);

    return $photo;
  }
}
