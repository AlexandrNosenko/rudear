<?php
use OAuth\Common\Storage\Session;

return [ 
  
  /*
  |--------------------------------------------------------------------------
  | oAuth Config
  |--------------------------------------------------------------------------
  */

  /**
   * Storage
   */
  'storage' => new Session(), 

  /**
   * Consumers
   */
  'consumers' => [

    /**
     * Facebook
     */
    'Facebook' => [
      'client_id'     => env('FACEBOOK_ID'),
      'client_secret' => env('FACEBOOK_SECRET'),
      'scope'         => [],
    ],
    'Vkontakte' => [
      'client_id'     => "4801456",
      'client_secret' => '465oktiPyL8fTIdhz8ZE',
      'scope'         => ['photos']
    ]

  ]

];