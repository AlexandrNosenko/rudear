<div class="chat-left-inner">
  <div class="form-material">
    <input class="form-control p-20" type="text" placeholder="@lang('Search Contact')">
  </div>
  <ul class="chatonline style-none ">
    @foreach($chats as $chat)
      <li class="chat-item" data-chat-id="{{ $chat->id }}" data-receiver-id="{{ $chat->decideReceiver(Auth::user()->id)->id }}">
        <a>
          <img src="{{ $chat->decideReceiver(Auth::user()->id)->mainPhoto() }}" alt="avatar" class="img-circle">
          <span>
            {{ $chat->decideReceiver(Auth::user()->id)->name }} 
            {{ $chat->decideReceiver(Auth::user()->id)->email }}
            @include('profiles/_online_status', ['user' => $chat->decideReceiver(Auth::user()->id)])
          </span>
        </a>
      </li>
    @endforeach
    <li class="p-20"></li>
  </ul>
</div>