@foreach($chat->messages() as $message)
  <li class="message-container {{ $message->sender_id == Auth::user()->id ? 'odd' : '' }}">
    @if( !$message->isOwnedBy(Auth::user()->id) )
      <div class="chat-image">
        <img alt="Avatar" src="{{ $chat->decideReceiver(Auth::user()->id)->mainPhoto() }}" />
      </div>    
    @endif
    <div class="chat-body">
      <div class="chat-text">
        @if( !$message->isOwnedBy(Auth::user()->id) )
          <h4>{{ $message->chat()->decideReceiver(Auth::user()->id)->name}}</h4>
        @endif
        <p>{{ $message->text }}</p>
        <b>{{ date("H:m",strtotime($message->created_at)) }}</b>
      </div>
    </div>
  </li>
@endforeach