@include ('footer')
@section('assets')
  <!-- <script type="text/javascript">
    window.addEventListener('load', function() {
      oSpP.push("user_email","{{ Auth::user()->email }}");
    });
  </script> -->
  <link href="{{ asset('css/style2.css') }}" rel="stylesheet">
  <link href="{{ asset('css/chats.css') }}" rel="stylesheet">

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-slimScroll/1.3.8/jquery.slimscroll.min.js"></script>
  <script src="{{ mix('js/chats.js') }}"></script>

@endsection

@extends('layouts.app')

@section('content')
<div id="chats"></div>
@endsection
