<div class="row send-chat-box">
  {{ Form::open( array( 'url' => route('messages'), 'method' => 'post', 'class' => "col-sm-12 message-input" ) ) }}
    {{ Form::hidden('message[sender_id]', Auth::user()->id, ['id' => 'message-sender']) }}
    @if( isset($chats[0]) )
      {{ Form::hidden('message[receiver_id]', $chats[0]->decideReceiver(Auth::user()->id)->id, ['id' => 'message-receiver']) }}
    @endif
    {{ Form::textarea('message[text]', null, ['id' => 'chat-form-input', 'class' => "form-control", 'placeholder' => 'Type your message']) }}
    <div class="custom-send">
      <a class="cst-icon" data-toggle="tooltip" title="Insert Emojis">
        <i class="ti-face-smile"></i>
      </a>
      <a class="cst-icon" data-toggle="tooltip" title="File Attachment">
        <i class="fa fa-paperclip"></i>
      </a>
      <button id="chat-form-submit" class="btn btn-danger btn-rounded" type="button">Send</button>
    </div>
  {{ Form::close() }}
</div>