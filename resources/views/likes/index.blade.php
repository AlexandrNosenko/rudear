@section('assets')
@endsection

@extends('layouts.app')

@section('content')
<section class="likes">
  @foreach($likes as $like)
    <div class="like">
      <div class="type">{{ $like->event_name }}</div>
      <div class="title">{{ $like->triggered_by()->name }}</div>
    </div>
  @endforeach
</section>
@endsection
