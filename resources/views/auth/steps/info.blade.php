@section('assets')
    <script>
        var selectedCountry = "{{ substr($user->country, 0, 2) }}";
    </script>

    <script src="/js/references.js"></script>
    <script src="/js/info.js"></script>
    <link href="{{ asset('css/info.css') }}" rel="stylesheet">

    <script src="{{ asset('js/profiles.js') }}"></script>
    <link href="{{ asset('css/profiles.css') }}" rel="stylesheet">
@endsection

@extends('layouts.app')

@section('content')
<!--=================================
 inner-intro-->

<section class="inner-intro bg bg-fixed bg-overlay-black-60">
    <div class="container">
        <div class="row intro-title text-center">
            <div class="col-sm-12">
                <div class="section-title">
                    <h1 class="pos-r divider">Info <span class="sub-title">Info</span></h1>
                </div>
            </div>
        </div>
     </div>
</section>

<!--=================================
 inner-intro-->


<!--=================================
 login-->

<section class="page-section-ptb text-white" style="background: url(/images/pattern/04.png) no-repeat 0 0; background-size: cover;">
    <div class="container">
        <div class="row">
            <div class="col-md-offset-2 col-md-8">  
                @include('auth/steps/_step-breadcrumb')
            </div>
            {{ Form::open( array( 'url' => route('profile-update-info'), 'method' => 'patch', 'id'=>"user-info-form", 'class' => "main-form step-form" ) ) }}
                <section class="vip-access">
                    <div class="title-wrapper">
                        <span class="title divider-3 mb-30">Get Free Vip For 3 days</span>
                    </div>
                    @if( $user->status == 'vip' )
                        Activated
                    @else
                        <a href="/vip-statuses/trial" class="button btn-theme full-rounded btn btn-lg mt-20 animated right-icn">
                            <span>Get Now<i class="glyph-icon flaticon-hearts" aria-hidden="true"></i></span>
                        </a>
                    @endif
                    <div class="admin-hack">
                        {{ Form::checkbox('status', 'vip', $user->status == 'vip') }}
                        {{ Form::label('status', 'Vip status') }}
                        {{ Form::checkbox('status', 'normal', $user->status == 'normal') }}
                        {{ Form::label('status', 'normal status') }}
                    </div>
                </section> 
                <section class="about-me">
                    <div class="title-wrapper">
                        <span class="title divider-3 mb-30">About me</span>
                    </div>
                    <div class="form-group">
                        {{ Form::label('user[birth_date]', 'Birth Date') }}
                        <div class="input-group">
                            {{ Form::date( 'user[birth_date]', $user->birth_date, array('placeholder' => "BirthDate", 'class' => "form-control") ) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('zodiac', 'Zodiac') }}
                        <div class="input-group">
                            {{ Form::text( 'user_infos[zodiac]', $user->infos()->zodiac, array('placeholder' => "Zodiac", 'class' => "form-control", 'disabled' => 'true') ) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('education', 'Education') }}
                        <div class="input-group">
                            {{ Form::text( 'user_infos[education]', $user->infos()->education, array('placeholder' => "Education", 'class' => "form-control") ) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('occupation', 'Occupation') }}
                        <div class="input-group">
                            {{ Form::text( 'user_infos[occupation]', $user->infos()->occupation, array('placeholder' => "Occupation", 'class' => "form-control") ) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('lang', 'Languages') }}
                        <div class="input-group">
                            {{ Form::text( 'user_infos[lang]', $user->infos()->lang, array('placeholder' => "Languages", 'class' => "form-control") ) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('relations', 'Relations') }}
                        <div class="input-group">
                            {{ Form::text( 'user_infos[relations]', $user->infos()->relations, array('placeholder' => "Relations", 'class' => "form-control") ) }}
                        </div>
                    </div>            
                    <div class="form-group">
                        {{ Form::label('children', 'Children') }}
                        <div class="input-group">
                            {{ Form::text( 'user_infos[children]', $user->infos()->children, array('placeholder' => "Children", 'class' => "form-control") ) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('slogan', 'Text near main photo') }}
                        <div class="input-group">
                            {{ Form::text( 'user_infos[slogan]', $user->infos()->slogan, array('placeholder' => "Text near main photo", 'class' => "form-control") ) }}
                        </div>
                    </div>  
                </section>
                <section>
                    <div class="title-wrapper">
                        <span class="title divider-3 mb-30">Your Country</span>
                    </div>
                    <div class="form-group">
                        <div class="bfh-selectbox bfh-countries bootstrap-select" data-country="{{ $user->country }}" data-flags="true">
                          <input type="hidden" name="user[country]" value="{{ $user->country }}">
                          <a class="bfh-selectbox-toggle" role="button" data-toggle="bfh-selectbox" href="#">
                            <span class="bfh-selectbox-option input-medium" data-option=""></span>
                            <b class="caret"></b>
                          </a>
                          <div class="bfh-selectbox-options">
                            <input type="text" class="bfh-selectbox-filter">
                            <div role="listbox">
                            <ul role="option">
                            </ul>
                            </div>
                          </div>
                        </div>
                    </div>
                </section>
                <section>
                    <div class="title-wrapper">
                        <span class="title divider-3 mb-30">Gender</span>
                    </div>
                    <div class="form-group full-width">
                        <div class="col-sm-4">
                            <div class="radio">
                                <input name="user[gender]" type="radio" value="female" id="gender_female" {{ $user->gender == 'male' ? '' : 'checked'}}>
                                <label for="gender_female">female</label>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="radio">
                                <input name="user[gender]" type="radio" value="male" id="gender_male" {{ $user->gender == 'male' ? 'checked' : ''}}>
                                <label for="gender_male">male</label>
                            </div>
                        </div>    
                    </div>
                </section>
                <section>
                    <div class="title-wrapper">
                        <span class="title divider-3 mb-30">Bad habits</span>
                    </div>
                    <div class="form-group full-width">
                        <div class="col-sm-4">
                            <div class="radio">
                                <input name="user_infos[drinker]" value="0" type="radio" id="drinker_no" {{ $user->infos()->drinker ? '' : 'checked'}}>
                                <label for="drinker_no">Don't Drink</label>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="radio">
                                <input name="user_infos[drinker]" value="1" type="radio" id="drinker_yes" {{ $user->infos()->drinker ? 'checked' : ''}}>
                                <label for="drinker_yes">Drink</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group full-width">
                        <div class="col-sm-4">
                            <div class="radio">
                                <input name="user_infos[smoker]" value="0" type="radio" id="smoker_no" {{ $user->infos()->smoker ? '' : 'checked'}}>
                                <label for="smoker_no">Don't smoke</label>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="radio">
                                <input name="user_infos[smoker]" value="1" type="radio" id="smoker_yes" {{ $user->infos()->smoker ? 'checked' : ''}}>
                                <label for="smoker_yes">Smoke</label>
                            </div>
                        </div>
                    </div>
                </section>
                <section>
                    <div class="form-group">
                        <textarea name="user[bio]" class="form-control user-bio" rows="3" placeholder="@lang('View words about me')">{{ $user->bio }}</textarea>
                    </div>                    
                </section>
                <section>
                    <div class="title-wrapper">
                        <span class="title divider-3 mb-30">Refference Link</span>
                    </div>
                    <div class="form-group inline-field">
                        <div class="input-group copy-link-field">
                            <span class="reference-link">{{ Request::root() }}/r/</span>
                            {{ Form::text( 'reference[value]', $user->referenceLink()->value, array('placeholder' => "Education", 'class' => "link-value") ) }}
                        </div>
                        <div class="copy-link-btn">
                            <i class="fa fa-clipboard" aria-hidden="true"></i>Copy
                        </div>
                    </div>

                   <!--  <div class="form-group">
                        <div class="profile-info">
                            <p class="mb-0"><i class="fa fa-info-circle" aria-hidden="true"></i> by clicking submit you are agreeing to our terms and conditions of use.</p>
                        </div>
                    </div> -->
                </section>
                
                <div class="form-group text-center">
                    <div class="button btn-theme full-rounded btn btn-lg mt-20 animated right-icn">
                        <span>Save<i class="glyph-icon flaticon-hearts" aria-hidden="true"></i></span>
                    </div>
                </div>
            {{ Form::close() }}
            <!-- <div class="row setup-content" id="step-1">
                <div class="col-md-12">
                    <h4 class="title divider-3 mb-50">Photo</h4>
                    
                    <div class="form-group mb-0">
                        <a href="{{ route('register-info') }}" class="button btn-theme full-rounded btn btn-lg mt-20 animated right-icn">
                            <span>Next<i class="glyph-icon flaticon-hearts" aria-hidden="true"></i></span>
                        </a>
                    </div>
                </div>
            </div> -->
        </div>
    </div>
</section>
@endsection