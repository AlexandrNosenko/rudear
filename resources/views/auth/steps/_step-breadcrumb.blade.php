<div class="stepwizard">
    <div class="stepwizard-row setup-panel">
        <div class="stepwizard-step">
            <a href="{{ route('register-photos') }}" class="btn btn-circle {{ Request::is('*/photos') ? 'active' : '' }}">
                1 <span><i class="fa fa-check" aria-hidden="true"></i></span>
            </a>
            <p>Photo</p>
        </div>
        <div class="stepwizard-step">
            <a href="{{ route('register-info') }}" class="btn btn-circle {{ Request::is('*/info') ? 'active' : '' }}">
                2 <span><i class="fa fa-check" aria-hidden="true"></i></span>
            </a>
            <p>General info</p>
        </div>
        <div class="stepwizard-step">
            <!--  disabled="disabled" -->
            <a href="{{ route('register-preferences') }}" class="btn btn-circle {{ Request::is('*/preferences') ? 'active' : '' }}">
                3 <span><i class="fa fa-check" aria-hidden="true"></i></span>
            </a>
            <p>preferences</p>
        </div>
    </div>
</div>