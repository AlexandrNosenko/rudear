@section('assets')
    <script src="/js/info.js"></script>
    <link href="{{ asset('css/info.css') }}" rel="stylesheet">
@endsection

@extends('layouts.app')

@section('content')
<!--=================================
 inner-intro-->

<section class="inner-intro bg bg-fixed bg-overlay-black-60">
    <div class="container">
        <div class="row intro-title text-center">
            <div class="col-sm-12">
                <div class="section-title">
                    <h1 class="pos-r divider">Preferences<span class="sub-title">Preferences</span></h1>
                </div>
            </div>
        </div>
     </div>
</section>

<!--=================================
 inner-intro-->


<!--=================================
 login-->
<section class="page-section-ptb text-white" style="background: url(/images/pattern/04.png) no-repeat 0 0; background-size: cover;">
    <div class="container">
        <div class="row">
            <div class="col-md-offset-2 col-md-8">  
                @include('auth/steps/_step-breadcrumb')
            </div>
            {{ Form::open( array( 'url' => route('profile-update-pref'), 'method' => 'patch', 'id'=>"user-info-form", 'class' => "main-form step-form" ) ) }}
                <section class="about-me">
                    <div class="title-wrapper">
                        <span class="title divider-3 mb-30">Intrested in</span>
                    </div>
                    <div class="form-group full-width">
                        {{ Form::hidden('user_infos[pref_gender]', $user->infos()->pref_gender) }}
                        <div class="col-sm-4">
                            <div class="radio">
                                <input name="_pref_gender" type="radio" value="female" id="gender_female" {{ $user->infos()->pref_gender == 'female' ? 'checked' : ''}}>
                                <label for="gender_female">female</label>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="radio">
                                <input name="_pref_gender" type="radio" value="male" id="gender_male" {{ $user->infos()->pref_gender == 'male' ? 'checked' : ''}}>
                                <label for="gender_male">male</label>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="radio">
                                <input name="_pref_gender" type="radio" value="both" id="gender_both" {{ $user->infos()->pref_gender == 'both' ? 'checked' : ''}}>
                                <label for="gender_both">both</label>
                            </div>
                        </div>
                    </div>
                </section>
                <section>
                    <div class="title-wrapper">
                        <span class="title divider-3 mb-30">Age Preference</span>
                    </div>
                    {{ Form::hidden('user_infos[pref_age]', $user->infos()->pref_age) }}
                    <div class="form-group">
                        <div>
                            <input 
                              id="pre-age-min" 
                              class="range-slider" 
                              data-slider-id='ex1Slider1' 
                              type="text" 
                              data-slider-min="0" 
                              data-slider-max="30" 
                              data-slider-step="1" 
                              data-slider-value="{{ $user->infos()->prefAgeMin() }}"
                              />
                            <input 
                              type="text" 
                              id="pre-age-max" 
                              class="range-slider" 
                              data-slider-id='ex1Slider2' 
                              data-slider-min="18" 
                              data-slider-max="100" 
                              data-slider-step="1" 
                              data-slider-value="{{ $user->infos()->prefAgeMax() }}"
                              />
                        </div>
                    </div>
                </section>
                <section>
                    <div class="title-wrapper">
                        <span class="title divider-3 mb-30">Here for</span>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-4">
                            <div class="checkbox">
                                {{ Form::checkbox( 'user_infos[pref_goal][]', 'friends', $user->infos()->haveGoal('friends'), array('id' => "goal-friends")) }}
                                <label for="goal-friends">Friends</label>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="checkbox">
                                {{ Form::checkbox( 'user_infos[pref_goal][]', 'boyfriend', $user->infos()->haveGoal('boyfriend'), array('id' => "goal-boyfriend")) }}
                                <label for="goal-boyfriend">Boyfriend</label>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="checkbox">
                                {{ Form::checkbox( 'user_infos[pref_goal][]', 'sex', $user->infos()->haveGoal('sex'), array('id' => "goal-sex")) }}
                                <label for="goal-sex">Sex</label>
                            </div>
                        </div>
                    </div>
                </section>
                <section>
                    <div class="title-wrapper">
                        <span class="title divider-3 mb-30">Country Preference</span>
                    </div>
                    {{ Form::text(null, null, array( 'data-type' => "contry-selector", 'data-name' => "user_infos[pref_country]", 'data-value' => $user->infos()->pref_country )) }}
                </section>
                <!-- <section>
                    <label class="title divider-3 mb-60">Distance Preference (miles)</label>
                    <div class="form-group">
                        <div>
                            <input id="slider3" class="range-slider" data-slider-id='ex1Slider1' type="text" data-slider-min="0" data-slider-max="35" data-slider-step="1" data-slider-value="30"/>
                            <input id="slider4" class="range-slider" data-slider-id='ex1Slider2' type="text" data-slider-min="36" data-slider-max="200" data-slider-step="1" data-slider-value="160"/>
                        </div>
                    </div>
                </section> -->
                
                <div class="form-group text-center">
                    <div class="button btn-theme full-rounded btn btn-lg mt-20 animated right-icn">
                        <span>Save<i class="glyph-icon flaticon-hearts" aria-hidden="true"></i></span>
                    </div>
                </div>
            {{ Form::close() }}
            <!-- <div class="row setup-content" id="step-1">
                <div class="col-md-12">
                    <h4 class="title divider-3 mb-50">Photo</h4>
                    
                    <div class="form-group mb-0">
                        <a href="{{ route('register-info') }}" class="button btn-theme full-rounded btn btn-lg mt-20 animated right-icn">
                            <span>Next<i class="glyph-icon flaticon-hearts" aria-hidden="true"></i></span>
                        </a>
                    </div>
                </div>
            </div> -->
        </div>
    </div>
</section>
@endsection
