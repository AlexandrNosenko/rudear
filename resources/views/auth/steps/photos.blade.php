@section('assets')
    <script src="/js/photos.js"></script>
@endsection

@extends('layouts.app')

@section('content')
<!--=================================
 inner-intro-->

<section class="inner-intro bg bg-fixed bg-overlay-black-60">
    <div class="container">
        <div class="row intro-title text-center">
            <div class="col-sm-12">
                <div class="section-title">
                    <h1 class="pos-r divider">Photos<span class="sub-title">Photos</span></h1>
                </div>
            </div>
        </div>
     </div>
</section>

<!--=================================
 inner-intro-->


<!--=================================
 login-->

<section class="page-section-ptb text-white" style="background: url(/images/pattern/04.png) no-repeat 0 0; background-size: cover;">
    <div class="container">
        <div class="row">
            <div class="col-md-offset-2 col-md-8">  
                <div class="step-form">
                    @include('auth/steps/_step-breadcrumb')
                    <div class="row setup-content" id="step-1">
                        <div class="col-md-12">
                            <h4 class="title divider-3 mb-50">Photo</h4>
                            <div class="row row-eq-height images-wrapper">
                                <div class="user-images">                                            
                                    @foreach ($photos as $key => $photo)
                                        <div class="user-image {{ $photo->type == 'main' ? 'main-image' : '' }}">
                                            <form class="delete-image-btn" method="POST" action="{{ route('photo', ['id' => $photo->id]) }}">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <button type="submit">x</button>
                                            </form>
                                            <img  src="{{ $photo->url() }}" alt="">
                                            <form class="main-image-toggler" class="text-center mt-30" method="POST" action="{{ route('photo', ['id' => $photo->id]) }}">
                                                {{ csrf_field() }}
                                                {{ method_field('PATCH') }}
                                                @if( $photo->type == 'main' )
                                                    {{ Form::hidden('type', 'normal' ) }}
                                                    <div class="favorite-toggle favorite-toggle-on"></div>
                                                @else
                                                    {{ Form::hidden('type', 'main' ) }}
                                                    <div class="favorite-toggle favorite-toggle-off"></div>
                                                @endif
                                            </form>
                                        </div>
                                    @endforeach
                                    @for ($i = count($photos) + 1; $i <= 6; $i++)
                                        <div class="user-image">
                                            <img class="image-placeholder" src="/images/step/0{{ $i }}.png" alt="">
                                        </div>
                                    @endfor
                                <!--<div class="row mb-10 row-eq-height">
                                        <div class="col-sm-8 pr-15">
                                            <img class="img-center full-width" src="/images/step/01.png" alt="">
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="row">
                                                <div class="col-sm-12 mb-30">
                                                </div>
                                                <div class="col-sm-12">
                                                    <img class="img-center" src="/images/step/03.png" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <iv class="row mt-30">
                                        <div class="col-sm-4">
                                            <img class="img-center" src="/images/step/04.png" alt="">
                                        </div>
                                        <div class="col-sm-4">
                                            <img class="img-center" src="/images/step/05.png" alt="">
                                        </div>
                                        <div class="col-sm-4">
                                            <img class="img-center" src="/images/step/06.png" alt="">
                                        </div>
                                    </div>-->
                                </div>
                            </div>
                            <form id="user-images-form" class="text-center mt-30" method="POST" action="{{ route('photos') }}"  enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="file" name="photos" style="display: none;"/>
                                <div class="form-group">
                                    <div class="image-upload-btn step-social mt-30 mb-20 text-center clearfix">
                                        <ul class="list-inline text-capitalize">
                                            <!-- <li>
                                                <div class="image-upload-btn">
                                                    <i class="fa fa-facebook-official" aria-hidden="true"></i>Select
                                                </div>
                                            </li> -->
                                            <li>
                                                <div class="image-info">
                                                    <i class="fa fa-file-image-o" aria-hidden="true"></i>Select
                                                    <span class="file-name"></span>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="step-social upload-btn mt-30 mb-20 text-center clearfix">Upload</div>
                                </div>
                                <div class="form-group">
                                    <div class="profile-info">
                                        <p class="mb-0"><i class="fa fa-info-circle" aria-hidden="true"></i> profile with photos get 10 times as many responses</p>
                                    </div>
                                </div>
                            </form>
                            <div class="form-group mb-0">
                                <a href="{{ route('register-info') }}" class="button btn-theme full-rounded btn btn-lg mt-20 animated right-icn">
                                    <span>Next<i class="glyph-icon flaticon-hearts" aria-hidden="true"></i></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </div>
</section>
@endsection
