@extends('layouts.app')

@section('content')
<!--=================================
 inner-intro-->

<section class="inner-intro bg bg-fixed bg-overlay-black-60">
    <div class="container">
        <div class="row intro-title text-center">
            <div class="col-sm-12">
                <div class="section-title">
                    <h1 class="pos-r divider">@lang('Reset password')<span class="sub-title">@lang('Reset password')</span></h1>
                </div>
            </div>
        </div>
    </div>
</section>

<!--=================================
 inner-intro-->


<!--=================================
 login-->
<section class="login-form login-img dark-bg page-section-ptb100 pb-70" style="background: url(/images/pattern/04.png) no-repeat 0 0; background-size: cover;">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="login-1-form clearfix text-center">  
                    <h4 class="title divider-3 text-white">@lang('Reset password')</h4>
                    <form class="form-horizontal" method="POST" action="{{ url('/password/reset') }}">
                        {{ csrf_field() }}

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="section-field mb-30 {{ $errors->has('email') ? ' has-error' : '' }}">
                          <div class="field-widget">
                            <i class="fa fa-envelope-o" aria-hidden="true"></i>
                            <input id="email" class="email" type="email" placeholder="Email" name="email" value="{{ $email or old('email') }}" required>
                            @if ($errors->has('email'))
                              <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                              </span>
                            @endif
                          </div> 
                        </div>
                        

                        <div class="section-field mb-30 {{ $errors->has('password') ? ' has-error' : '' }}">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                            <div class="field-widget">
                                <i class="glyph-icon flaticon-padlock"></i>
                                <input id="Password" class="Password" type="password" placeholder="Password" name="password" required>
                            </div>
                        </div>
                        
                        <div class="section-field mb-30">
                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                            @endif
                          <div class="field-widget">
                           <i class="glyph-icon flaticon-padlock"></i>
                           <input id="ConfirmPassword" class="Password" type="password" placeholder="Confirm Password" name="password_confirmation" required>
                          </div> 
                        </div>

                        <div class="clearfix"></div>
                        
                        <div class="section-field text-uppercase text-center mt-20">
                            <button class="button btn-lg btn-theme full-rounded animated right-icn" type="submit">
                                <span>@lang('Reset password')<i class="glyph-icon flaticon-hearts" aria-hidden="true"></i></span>
                            </button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
