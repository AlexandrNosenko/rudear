@extends('layouts.app')

@section('content')
<!--=================================
 inner-intro-->
<section class="inner-intro bg bg-fixed bg-overlay-black-60">
    <div class="container">
        <div class="row intro-title text-center">
           <div class="col-sm-12">
        <div class="section-title"><h1 class="pos-r divider">@lang('register')<span class="sub-title">@lang('register')</span></h1></div>
        </div>           
        </div>
    </div>
</section>
<!--=================================
 inner-intro-->

<!--=================================
 login-->
<section class="login-form register-img dark-bg page-section-ptb">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="login-1-form register-1-form clearfix text-center">
                    <h4 class="title divider-3 text-white mb-30">@lang('sign up')</h4>

                    @include('auth/_social_buttons')
<h3>{{Cookie::get('ru_dear_referrer')}}</h3>
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}
                        {{ Form::hidden( 'referrer_link', Cookie::get('ru_dear_referrer') ) }}
                        <div class="section-field mb-30 {{ $errors->has('name') ? ' has-error' : '' }}">
                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                            <div class="field-widget">
                                <i class="glyph-icon flaticon-user"></i>
                                <input id="Firstname" id="name" type="text" name="name" placeholder="First name" value="{{ old('name') }}" required autofocus>
                            </div> 
                        </div>
            
                        <!-- <div class="section-field mb-30">
                          <div class="field-widget">
                           <i class="glyph-icon flaticon-user"></i>
                           <input id="Lastname" type="text" placeholder="Last name">
                          </div> 
                        </div> -->

                        <div class="section-field mb-30 {{ $errors->has('email') ? ' has-error' : '' }}">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                            <div class="field-widget">
                                <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                <input id="email" class="email" type="email" placeholder="Email" name="email" value="{{ old('email') }}" required>
                            </div> 
                        </div>

                        <div class="section-field mb-30 {{ $errors->has('password') ? ' has-error' : '' }}">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                            <div class="field-widget">
                                <i class="glyph-icon flaticon-padlock"></i>
                                <input id="Password" class="Password" type="password" placeholder="{{ __('Password') }}" name="password" required>
                            </div>
                        </div>
                        
                        <div class="section-field mb-30">
                          <div class="field-widget">
                           <i class="glyph-icon flaticon-padlock"></i>
                           <input id="ConfirmPassword" class="Password" type="password" placeholder="{{ __('Confirm Password') }}" name="password_confirmation" required>
                          </div> 
                        </div>

               
                    <!-- <div class="section-field mb-30">
                      <div class="field-widget">
                       <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                       <input id="Collagename" type="text" placeholder="Collage name">
                      </div> 
                    </div>
                                <div class="section-field mb-30">
                      <div class="field-widget">
                       <i class="fa fa-briefcase" aria-hidden="true"></i>
                       <input id="Proffesionname" type="text" placeholder="Proffesion">
                      </div> 
                    </div> -->

                    <div class="clearfix"></div>
                    <div class="section-field text-uppercase text-center mt-20">
                        <button class="button btn-lg btn-theme full-rounded animated right-icn" type="submit">
                            <span>@lang('Next')<i class="glyph-icon flaticon-hearts" aria-hidden="true"></i></span>
                        </button>
                    </div>
                    <div class="clearfix"></div>
                    <div class="section-field mt-20 text-center text-white">
                        <p class="lead mb-0">@lang('Have an account?')<a class="text-white" href="{{ route('login') }}"><u>@lang('sign in')!</u> </a></p>
                    </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</section>
@endsection
