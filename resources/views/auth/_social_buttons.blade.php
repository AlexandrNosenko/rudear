<div class="login-1-social mt-50 mb-50 text-center clearfix">
    <a class="fb" href="{{ url('/auth/facebook') }}"><i class="fa fa-facebook"></i> Facebook</a>
    <a class="gplus" href="#"><i class="fa fa-google-plus"></i>  google+</a>
    <a class="vk" href="{{ url('/auth/vkontakte') }}" class="btn btn-vkontakte"><i class="fa fa-vkontakte"></i> Vkontakte</a>
</div>