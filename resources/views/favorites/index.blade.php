@section('assets')
  <link href="{{ asset('css/favorites.css') }}" rel="stylesheet">

  <!-- <script src="{{ asset('js/favorites.js') }}"></script> -->
@endsection

@extends('layouts.app')

@section('content')
<section class="favorites">
    @foreach($favoriteUsers as $user)
        <div class="favorite-user">
            {{ $user->name }}
        </div>
    @endforeach
</section>
@endsection
