<!--=================================
header -->
<header id="header" class="dark">
  <!--=================================
   mega menu -->
  <div class="menu">
    <!-- menu start -->
    <nav id="menu" class="mega-menu">
      <!-- menu list items container -->
      <section class="menu-list-items">
        <div class="container">
          <div class="row">
            <div class="col-lg-12 col-md-12">
              <!-- menu logo -->
              <ul class="menu-logo">
                <li>
                  <a href="{{ url('/') }}"><img src="/images/logo.png" alt="logo"></a>
                </li>
              </ul>
              <!-- menu links -->
              <ul class="menu-links">
                <!-- active class -->
                <li class="{{ Request::is('/') ? 'active' : '' }}">
                  <a href="/"> Home </a>
                </li>
                <li class="{{ Request::is('profiles') ? 'active' : '' }}">
                  <a href="{{ route('profiles') }}">Profiles</a>
                </li>
                @guest
                  <li class="{{ Request::is('login') ? 'active' : '' }}"><a href="{{ route('login') }}">Login</a></li>
                  <li class="{{ Request::is('register') ? 'active' : '' }}"><a href="{{ route('register') }}">Register</a></li>
                @else
                  <li class="active"><a href="javascript:void(0)"> {{ Auth::user()->name }} <i class="fa fa-angle-down fa-indicator"></i></a>
                    <!-- drop down multilevel  -->
                    <ul class="drop-down-multilevel right-menu">
                      <li>
                        <a href="{{ route('register-photos') }}">@lang('My Account')</a>
                      </li>
                      <li>
                        <a href="{{ route('chats') }}">@lang('My Chats')</a>
                      </li>
                      <li>
                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                          @lang('Logout')
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                          {{ csrf_field() }}
                        </form>
                      </li>
                    </ul>
                  </li>
                @endguest
              </ul>
            </div>
          </div>
        </div>
      </section>
    </nav>
    <!-- menu end -->
  </div>
</header>
<!--=================================
 header -->