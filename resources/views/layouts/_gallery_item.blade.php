<figure class="gallery-item {{ $item_class }}" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
  <a href="{{ $photo->url() }}" itemprop="contentUrl" data-size="{{ $photo->dimensions() }}">
    <img data-user-avatar="{{ $photo->type == 'main' ? 'true' : 'false' }}" src="{{ $photo->url() }}" itemprop="thumbnail" alt="Image description" />
  </a>
</figure>