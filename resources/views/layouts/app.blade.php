<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- <meta name="keywords" content="HTML5 Template" /> -->
        <title>TheCupidone.com | Find you true love</title>
        <meta name="description" content="TheCupidone.com | Find you true love" />
        <!-- <meta name="author" content="potenzaglobalsolutions.com" /> -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <meta property="og:title" content="TheCupidone.com | Find you true love"/>
        <meta property="og:image" content="https://thecupidone.com/social-share-preview.png"/>
        <meta property="og:site_name" content="TheCupidone.com"/>
        <meta property="og:description" content="Best Dating site this world has ever seen."/>

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Favicon -->
        <link rel="shortcut icon" href="images/favicon.ico" />

        <!-- Styles -->

        <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
        <link href="/css/bootstrap-slider.min.css" rel="stylesheet" type="text/css" />
        <link href="https://js.nicdn.de/bootstrap/formhelpers/docs/assets/css/bootstrap-formhelpers.css" rel="stylesheet">
        <link href="https://js.nicdn.de/bootstrap/formhelpers/docs/assets/css/bootstrap-formhelpers-countries.flags.css" rel="stylesheet">

        <!-- Toaster -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
        <!-- mega menu -->
        <link href="/css/mega-menu/mega_menu.css" rel="stylesheet" type="text/css" />
        <!-- font-awesome -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
        <!-- Flaticon -->
        <link href="/css/flaticon.css" rel="stylesheet" type="text/css" />
        <!-- Magnific popup -->
        <link href="/css/magnific-popup/magnific-popup.css" rel="stylesheet" type="text/css" />
        <!-- owl-carousel -->
        <link href="/css/owl-carousel/owl.carousel.css" rel="stylesheet" type="text/css" />
        <!-- General style -->
        <link href="/css/general.css" rel="stylesheet" type="text/css" />
        <!-- main style -->
        <link href="/css/style.css" rel="stylesheet" type="text/css" />


        <!-- Animate -->
        <link rel="stylesheet" type="text/css" href="/css/animate.min.css">


        <!-- jquery  -->
        <script type="text/javascript" src="/js/jquery.min.js"></script>
        <!-- bootstrap -->
        <script type="text/javascript" src="/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="/js/bootstrap-select.min.js"></script>
        <!-- Toaster -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
        <!-- Push notifications -->
        <!-- <script charset="UTF-8" src="//cdn.sendpulse.com/9dae6d62c816560a842268bde2cd317d/js/push/394e380e4e48893ca3ee80255b67c967_1.js" async></script>         -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        @yield('assets')
    </head>
    <body>
        <!--=================================
         preloader -->

        <div id="preloader">
          <div class="clear-loading loading-effect"><img src="/images/loading.gif" alt=""></div>
        </div>

        <!--=================================
         preloader -->

        @include('layouts/_navigation')
        <main>
          @include('layouts/_flashes', compact('errors'))
          @yield('content')
        </main>

        <!--=================================
        footer -->
        <footer class="text-white text-center" >
          <div class="footer-widget">
            <div class="container">
              <div class="row">
                <div class="col-md-offset-2 col-md-8">
                  <div class="footer-logo mb-20">
                    <img class="img-center" src="images/footer-logo.png" alt="">
                  </div>
                  <ul class="languages">
                    @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                      <li>
                        <a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                          {{ $properties['native'] }}
                        </a>
                      </li>
                    @endforeach
                  </ul>
                  <div class="social-icons color-hover mb-10">
                    <ul>
                      <li class="social-facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                      <li class="social-twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                      <li class="social-dribbble"><a href="#"><i class="fa fa-dribbble"></i></a></li>
                      <li class="social-gplus"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                      <li class="social-youtube"><a href="#"><i class="fa fa-youtube"></i></a></li>
                    </ul>
                  </div>
                  <p class="text-white">© 2017  - Cupid Love All Right Reserved </p>
                </div>
              </div>
            </div>
          </div>
        </footer>

        <!--=================================
        footer -->

        <div id="back-to-top"><a class="top arrow" href="#top"><i class="fa fa-level-up"></i></a></div>


        <!-- appear -->
        <script type="text/javascript" src="/js/jquery.appear.js"></script>
        <!-- Menu -->
        <script type="text/javascript" src="/js/mega-menu/mega_menu.js"></script>
        <!-- owl-carousel -->
        <script type="text/javascript" src="/js/owl-carousel/owl.carousel.min.js"></script>
        <!-- counter -->
        <script type="text/javascript" src="/js/counter/jquery.countTo.js"></script>
        <!-- Magnific Popup -->
        <script type="text/javascript" src="/js/magnific-popup/jquery.magnific-popup.min.js"></script>
        <!-- Bootstrap slider -->
        <script type="text/javascript" src="/js/bootstrap-slider.min.js"></script>
        <!-- Bootstrap country select -->
        <script src="https://js.nicdn.de/bootstrap/formhelpers/docs/assets/js/bootstrap-formhelpers-selectbox.js"></script>
        <script src="{{ asset('js/countryPicker/bootstrap-formhelpers-countries.en_US.js') }}"></script>
        <script src="https://js.nicdn.de/bootstrap/formhelpers/docs/assets/js/bootstrap-formhelpers-countries.js"></script>

        <!-- Custom scripts -->
        <script src="{{ asset('js/custom.js') }}"></script>
        <script src="{{ asset('js/app.js') }}"></script>
        @if( \Auth::user() != null )
            <script>
                // Browser Notifcation
                function handleMessageNotification(data){
                  data.received = !(data.user.id == {{ \Auth::user()->id }});
                  if( data.received ){
                    var notification = new Notification(`New message from ${data.user.name}`, {
                      icon: window.location.origin + data.user.avatar,
                      body: "click to view",
                      link: window.location.origin +'/chats'
                    });

                    notification.onclick = function () {
                      window.open(window.location.origin +'/chats');
                    };
                  }
                }
            </script>
            @foreach(App\Chat::with(\Auth::user()->id) as $chat)
                <script>
                    console.log(`chatroom{{ $chat->id}}`)
                    window.EchoInst.channel(`chatroom{{ $chat->id}}`)
                      .listen('MessagePosted', data => handleMessageNotification(data));
                </script>
            @endforeach
        @endif
    </body>
</html>
