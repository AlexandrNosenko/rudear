@foreach($errors->all() as $error)
  <script>
    toastr.error('{{ $error }}')
  </script>
@endforeach

@foreach (['danger', 'warning', 'success', 'info', 'status'] as $key)
  @if( Session::has($key) && in_array($key, ['info', 'status']) )
    <script>
      toastr.info('{{ Session::get($key) }}')
    </script>
  @elseif(Session::has($key) && in_array($key, ['danger', 'warning']))
    <script>
      toastr.error('{{ Session::get($key) }}')
    </script>
  @elseif(Session::has($key))
    <script>
      toastr.success('{{ Session::get($key) }}')
    </script>
  @endif
@endforeach
