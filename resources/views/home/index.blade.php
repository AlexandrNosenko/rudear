@extends('layouts.app')
@section('assets')
  <link rel="stylesheet" href="{{ asset('css/home.css') }}">
  <script src="{{ asset('js/jquery.steps.min.js') }}"></script>
  <script>
    
    function wizzardNext(){
      $('a[href="#next"]').click()
    }
    function wizzardFinish(){
      $('.form-inner').submit();
    }
    $(document).on('ready', function(){
      $('.form-inner').steps({
          headerTag: "span",
          bodyTag: "section",
          transitionEffect: "slideLeft",
          autoFocus: true
      });  
    })
  </script>
@endsection
@section('content')
<!--=================================
 banner -->
<section class="header-container">
  <div class="banner banner-3 bg-1" style="background-image: url(images/bg/bg-6.jpg);" data-gradient-red="4">
    <div class="ver-center">
      <div class="container">
        <div class="row valign">
          <div class="col-md-7 col-sm-12" data-valign-overlay="middle">     
            <h1 class="animated3 text-white">@lang('choose Your soul Mate from 100,000+ lonely hearts')</h1>
          </div>
          <div class="col-md-5 col-sm-8 col-xs-12">  
            <div class="banner-form">
              <h4>@lang('dating with Cupid love Your perfect match is just a click away')</h4>
              {{ Form::open( array( 'url' => route('homepage-register'), 'method' => 'post', 'class' => "form-inner" ) ) }}
                <span></span>
                <section>
                  <div class="form-group">
                    <label class="col-sm-4 col-xs-4 control-label">@lang('I am a')</label>
                    <div class="col-sm-8 col-xs-8">
                      {{ Form::select('user[gender]', ['male'=> 'Man', 'female' => 'Woman'], null, [ 'class' => "selectpicker" ]) }}
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-4 col-xs-4 control-label">@lang('Seeking a')</label>
                    <div class="col-sm-8 col-xs-8">
                      {{ Form::select('user_infos[pref_gender]', ['female' => 'Woman', 'male'=> 'Man', 'both' => 'Both'], null, [ 'class' => "selectpicker" ]) }}
                    </div>
                  </div>

                  <div class="form-group text-center">
                    <div onClick="wizzardNext()" class="button btn-theme full-rounded btn btn-lg mt-20 animated right-icn">
                      <span>@lang('Next')<i class="glyph-icon flaticon-hearts" aria-hidden="true"></i></span>
                    </div>
                  </div>
                </section>
                <span></span>
                <section>
                  <div class="form-group">
                    <label class="col-sm-4 col-xs-4 control-label">@lang('My name is')</label>
                    <div class="input-group col-sm-8 col-xs-8">
                        {{ Form::text('user[name]', null, [ 'class' => "form-control" ]) }}
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="col-sm-4 col-xs-4 control-label">@lang('My email is')</label>
                    <div class="input-group col-sm-8 col-xs-8">
                        {{ Form::text('email', null, [ 'class' => "form-control" ]) }}
                    </div>
                  </div>

                  <div class="form-group text-center">
                    <div onClick="wizzardFinish()" class="button btn-theme full-rounded btn btn-lg mt-20 animated right-icn">
                      <span>@lang('Next')<i class="glyph-icon flaticon-hearts" aria-hidden="true"></i></span>
                    </div>
                  </div>
                </section>
              {{ Form::close() }}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="form-1 ptb-20">
    <div class="container">
      @include('profiles/_search_form')
    </div>
  </div>
</section>

@include('profiles/_users_carosel', ['users' => $topUsers, 'blockTitle' => 'Vip Users'])

<section class="page-section-ptb grey-bg story-slider">
  <div class="container">
    <div class="row mb-20 xs-mb-0">
      <div class="col-md-offset-1 col-md-10 text-center">
        <h2 class="title divider">@lang('They Found True Love')</h2>
      </div>
    </div>
  </div>
  <div class="users-grid-block">
    @foreach( $latestUsers as $topUser)
      <a href="{{ route('profile', ['id' => $topUser->id ]) }}" class="item">
        <div class="story-item">
          <div class="story-image clearfix" style="background-image: url('{{ $topUser->mainPhoto() }}');">
            <!-- <img class="img-responsive" src="{{ $topUser->mainPhoto() }}"> -->
          </div>
          <div class="story-details text-center">
            <h5 class="title divider-3">{{ $topUser->name }}</h5>
            <!-- <div class="about-des mt-30">Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in</div> -->
          </div>
        </div>
      </a>
    @endforeach
    <a href="/login"><button class="button show-more-btn">More...</button></a>
  </div>
</section>

<!--=================================
 page-section -->

<section class="ptb-50 action-box-img bg text-center text-white bg-overlay-black-80" style="background-image:url(images/bg/bg-4.jpg)">
 <div class="container">
  <div class="row">
    <div class="col-sm-12">
        <h5 class="pb-20">@lang("Want to hear more story, subscribe for our newsletter")</h5>
        <a href="/register" class="button  btn-lg btn-theme full-rounded animated right-icn"><span>@lang("Subscribe")<i class="glyph-icon flaticon-hearts" aria-hidden="true"></i></span></a>
   </div>
  </div>
 </div>
</section>

@endsection
