@section('assets')
    <script src="/js/references.js"></script>
    <!-- <link href="{{ asset('css/info.css') }}" rel="stylesheet"> -->
@endsection

@extends('layouts.app')

@section('content')
<!--=================================
 inner-intro-->

<section class="inner-intro bg bg-fixed bg-overlay-black-60">
    <div class="container">
        <div class="row intro-title text-center">
            <div class="col-sm-12">
                <div class="section-title">
                    <h1 class="pos-r divider">Reference Link<span class="sub-title">Reference Link</span></h1>
                </div>
            </div>
        </div>
     </div>
</section>

<!--=================================
 inner-intro-->


<!--=================================
 login-->

<section class="page-section-ptb text-white" style="background: url(/images/pattern/04.png) no-repeat 0 0; background-size: cover;">
    <div class="container">
        <div class="row">
            {{ Form::open( array( 'url' => route('reference-links'), 'method' => 'post', 'class' => "main-form step-form" ) ) }}
                <div class="form-group">
                    <div class="input-group">
                        <span class="reference-link">{{ Request::root() }}/r/</span>
                        {{ Form::text( 'reference[value]', $user->referenceLink()->value, array('placeholder' => "Education", 'class' => "link-value") ) }}
                    </div>
                    <div class="copy-link-btn">Copy</div>
                </div>

                <div class="form-group text-center">
                    <div class="button btn-theme full-rounded btn btn-lg mt-20 animated right-icn">
                        <span>Save<i class="glyph-icon flaticon-hearts" aria-hidden="true"></i></span>
                    </div>
                </div>
            {{ Form::close() }}
        </div>
    </div>
</section>
@endsection