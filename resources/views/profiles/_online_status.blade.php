@if($user->isOnline())
  <small class="online text-success">Online</small><!-- text-warning -->
@else
  <small class="offline text-danger">Offline</small>
@endif