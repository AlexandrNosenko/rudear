@section('assets')
  <!-- Core CSS file -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.2/photoswipe.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.2/default-skin/default-skin.min.css">
  <link href="{{ asset('css/profiles.css') }}" rel="stylesheet">

  <!-- Core JS file -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.2/photoswipe.min.js"></script>
  <!-- UI JS file -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.2/photoswipe-ui-default.min.js"></script>
  <script src="{{ asset('js/photo-gallery.js') }}"></script>

  <script>
    $(document).ready(function(){
      initPhotoSwipeFromDOM('.photo-gallery');
      $('.profile-header-image').on('click', function(){$('[data-user-avatar="true"]').click()});
    })
  </script>
@endsection
@extends('layouts.app')

@section('content')
<section class="inner-intro details-page bg bg-fixed bg-overlay-black-20 pos-r" style="background-image:url(/images/bg/inner-bg-2.jpg);">
  <div class="container users-header">
    <div class="profile-cntn text-white">
      <ul>
        <li>
          <img class="profile-header-image" src="{{ $user->mainPhoto() }}">
          <div class="under-user-photo">
            @include('profiles/_online_status', ['user' => $user])
          </div>
        </li>
        <li>
          <div class="profile-text">
            <h2>{{ $user->name }}</h2>
            <h5>{{ $user->age() }} @lang('Years Old') {{ $user->livesIn() }}</h5>
            <h5>{{ $user->infos()->slogan }}</h5>
            @include('profiles/_vip_status', ['user' => $user])
          </div>
        </li>


        <li>
          {{ Form::open( array( 'url' => route('favorites'), 'method' => 'post' ) ) }}
            {{ Form::hidden('target_id', $user->id) }}
            <button class="button" type="submit">@lang('Add to favorite')</button>
          {{ Form::close() }}
        </li>

        <li>
          {{ Form::open( array( 'url' => route('black_list'), 'method' => 'post' ) ) }}
            {{ Form::hidden('target_id', $user->id) }}
            <button class="button" type="submit">@lang('Add to black list')</button>
          {{ Form::close() }}
        </li>

        <li>
          @if( $TODO_REFACTOR_send_msg_permission['ok'] )
              {{ Form::open( array( 'url' => route('create-chat'), 'method' => 'post' ) ) }}
                {{ Form::hidden('chat[receiver_id]', $user->id) }}
                <button class="button" type="submit">@lang('Write a message')</button>
              {{ Form::close() }}
          @elseif( in_array('vip', $TODO_REFACTOR_send_msg_permission['errors']) )
              @lang('You cannot write a message, get some vip')
              <button class="button btn-lg btn-theme full-rounded animated right-icn" type="submit">
                <span>@lang('Get Vip')<i class="glyph-icon flaticon-hearts" aria-hidden="true"></i></span>
              </button>
          @elseif( in_array('blacklist', $TODO_REFACTOR_send_msg_permission['errors']) )
              @lang("You've been blacklisted :< " )
          @endif
        </li>
      </ul>
    </div>
  </div>
</section>


<!--=================================
 banner -->

<section class="page-section-ptb pt-100 text-left">
  <div class="container info-container">
    <div class="sidebar">
      <div class="title-wrapper">
        <h4 class="title divider-3 mb-50">@lang('You might be interested in')</h4>
      </div>
      <section class="recommended-users">
        @foreach($recommendedUsers as $recommendedUser)
          <a href="{{ route('profile', ['id' => $recommendedUser->id ]) }}" class="recommended-user">
            <div class="avatar" style="background-image: url({{ $recommendedUser->mainPhoto() }});">
            </div>
            <div class="info">
              <span class="name">{{ $recommendedUser->name }}</span>
              <span class="name">{{ $recommendedUser->age() }} {{ $recommendedUser->livesIn() }}</span>
            </div>
          </a>
        @endforeach
      </section>
    </div>
    <div class="user-info">
      <section>
        <h4 class="title divider-3 mb-50">@lang('Basic Details')</h4>
        <span class="info-subtitle">@lang('I am looking for')</span>
        <div class="info">
          <span class="title">@lang('Wish to meet')</span>
          <div class="value">{{ showData($user->infos()->pref_gender) }}</div>
        </div>
        <div class="info">
          <span class="title">@lang('Purpose of dation')</span>
          <div class="value">{{ showData($user->infos()->pref_goal) }}</div>
        </div>
        <div class="info">
          <span class="title">@lang('Preferred age')</span>
          <div class="value">{{ showData($user->infos()->pref_age) }}</div>
        </div>
        <span class="info-subtitle">@lang('Details')</span>
        <div class="info">
          <span class="title">@lang('Height')</span>
          <div class="value">{{ showData($user->height) }}</div>
        </div>
        <div class="info">
          <span class="title">@lang('Weight')</span>
          <div class="value">{{ showData($user->weight) }}</div>
        </div>
        <div class="info">
          <span class="title">@lang('Hair color')</span>
          <div class="value">{{ showData($user->infos()->pref_age) }}</div>
        </div>
        <div class="info">
          <span class="title">@lang('Relationship')</span>
          <div class="value">{{ showData($user->infos()->relationship) }}</div>
        </div>
        <div class="info">
          <span class="title">@lang('Smoker')</span>
          <div class="value">{{ showData($user->infos()->smoker) }}</div>
        </div>
        <div class="info">
          <span class="title">@lang('Drinker')</span>
          <div class="value">{{ showData($user->infos()->drinker) }}</div>
        </div>
        <div class="info">
          <span class="title">@lang('Education')</span>
          <div class="value">{{ showData($user->infos()->education) }}</div>
        </div>
        <div class="info">
          <span class="title">@lang('Occupation')</span>
          <div class="value">{{ showData($user->infos()->occupation) }}</div>
        </div>
        <div class="info">
          <span class="title">@lang('Languages spoken')</span>
          <div class="value">{{ showData($user->infos()->langs) }}</div>
        </div>
        <div class="info">
          <span class="title">@lang('Children')</span>
          <div class="value">{{ showData($user->infos()->children) }}</div>
        </div>
        <div class="info">
          <span class="title">@lang('Place of residence')</span>
          <div class="value">{{ showData($user->livesIn()) }}</div>
        </div>
      </section>

      <section>
        <h4 class="title divider-3 mb-50">@lang('About Me')</h4>
        <div class="full-width">
          {{ $user->bio }}
        </div>
      </section>

      <section>
        <h4 class="title divider-3 mb-50">@lang('Photos')</h4>
        <div class="photo-gallery full-width" itemscope itemtype="http://schema.org/ImageGallery">
          @foreach($user->getPhotos() as $photo)
            @include('layouts/_gallery_item', array('photo' => $photo, 'item_class' => "" ))
          @endforeach
        </div>
      </section>
    </div>
  </div>
</section>
@include('layouts/_photo_gallery')
@endsection
