@section('assets')
  <script src="{{ asset('js/profiles.js') }}"></script>
  <link href="{{ asset('css/profiles.css') }}" rel="stylesheet">
@endsection

@extends('layouts.app')

@section('content')
<!--=================================
 banner -->

<section class="profiles-header inner-intro bg bg-fixed bg-overlay-black-60">
    <div class="container">
        <div class="row intro-title text-center">
            <div class="col-sm-12">
                <div class="section-title">
                    <h1 class="pos-r divider">@lang('profiles')<span class="sub-title">@lang('profiles')</span></h1>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="form-1 ptb-20">
  <div class="container">
    @include('profiles/_search_form')
  </div>
</section>
<!--=================================
 banner -->


<!--=================================
 Page Section -->

<section class="page-section-ptb profile-slider"> 
    <div class="container">
        <div class="row mb-50 xs-mb-30">
            <div class="col-sm-12 text-center">
                <h2 class="title divider">@lang('Vip Profiles')</h2>
            </div>
        </div>
        <div class="row">
            @foreach ($vipUsers as $user)
                <div class="col-sm-3 xs-mb-30">
                    @include('profiles/_list_item', array('user' => $user, 'elemClass' => 'profile-text'))
                </div>
            @endforeach
        </div>
    </div>
</section>

@include('profiles/_users_carosel', ['users' => $latestUsers, 'blockTitle' => 'Last Added Profiles'])

<section class="page-section-ptb pt-30 profile-slider"> 
    <div class="container">
        <div class="row mb-50 xs-mb-30">
            <div class="col-sm-12 text-center">
                <h2 class="title divider">@lang('All Profiles')</h2>
            </div>
        </div>
        <div class="all-users-container row mb-50 xs-mb-30" data-ajaxUrl="{{ route("profiles-ajax") }}">
            @include('profiles/_users_list', array('users' => $allUsers, 'elemClass' => 'profile-text'))
        </div>
        <div class="row">
            <div class="col-sm-12 text-center">
                <a class="button show-more-users btn-lg btn-theme full-rounded animated right-icn" style="height: 40px;">
                    <span>@lang('Show More')<i class="glyph-icon flaticon-hearts" aria-hidden="true"></i></span>
                </a>
            </div>
        </div>
    </div>
</section>
@endsection
