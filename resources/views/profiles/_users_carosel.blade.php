<section class="page-section-ptb profile-slider pb-30 xs-pb-60"> 
  <div class="container">
    <div class="row mb-20 xs-mb-0">
      <div class="col-md-offset-1 col-md-10 text-center">
        <h2 class="title divider">{{ $blockTitle }}</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="owl-carousel" data-nav-arrow="true" data-items="4" data-md-items="4" data-sm-items="3" data-xs-items="2" data-xx-items="1" data-space="30">
          @foreach ($users as $user)
            <div class="item">
              @include('profiles/_list_item', array('user' => $user, 'elemClass' => 'text-center'))
            </div>
          @endforeach
         </div>
       </div>
    </div>
  </div>
</section>