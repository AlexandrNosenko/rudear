@foreach ($users as $user)
    <div class="col-sm-3 xs-mb-30">
        @include('profiles/_list_item', array('user' => $user, 'elemClass' => $elemClass))
    </div>
@endforeach