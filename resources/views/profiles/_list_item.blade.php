<a href="{{ route('profile', ['id' => $user->id ]) }}" class="profile-item">
    <div class="profile-image clearfix">
        <!-- <img class="img-responsive" src="{{ $user->mainPhoto() }}" alt=""> -->
        <div class="img-responsive user-avatar" style="background-image: url('{{ $user->mainPhoto() }}');"> </div>
    </div>
    <div class="profile-details {{ $elemClass }}">
        <h5 class="title">{{ $user->name }}</h5>
        @include('profiles/_vip_status', ['user' => $user])
        <span class="{{ $elemClass == 'profile-text' ? 'text-black' : '' }}">
            @if ( isset($user->birth_date) )
                <span>{{ $user->age() }} Years Old</span>
            @else
                Cool age
            @endif
            @include('profiles/_online_status', ['user' => $user])
        </span>

    </div>
</a>