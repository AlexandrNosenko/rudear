{{ Form::open( array( 'url' => route('profiles'), 'method' => 'get', 'id'=>"filters-form", 'class' => "banner-form" ) ) }}
  <div class="row">
    <div class="col-sm-12 col-md-3">
      <div class="form-group">
            <label class="col-md-5 col-sm-12 control-label text-white">@lang('Looking for')</label>
            <div class="col-md-7 col-sm-12">
            {{ Form::select('filters[pref_gender]', 
                ['male' => 'Male', 'female' => 'female', 'both' => 'Both'], 
                $filters['pref_gender'], 
                array( 'class' => "selectpicker" )) }}
          </div>
        </div>
    </div>
    <div class="col-sm-4">
      <label class="col-md-2 col-sm-12 control-label text-white">@lang('Age')</label>
      <div class="form-group">
          <div>
              <input 
                type="text" 
                name="filters[pref_age_from]"
                id="pre-age-min" 
                class="range-slider" 
                data-slider-id='ex1Slider1' 
                data-slider-min="0" 
                data-slider-max="30" 
                data-slider-step="1" 
                data-slider-value="{{ $filters['pref_age_from'] }}"
                />
              <input 
                type="text" 
                name="filters[pref_age_to]"
                id="pre-age-max" 
                class="range-slider" 
                data-slider-id='ex1Slider2' 
                data-slider-min="18" 
                data-slider-max="100" 
                data-slider-step="1" 
                data-slider-value="{{ $filters['pref_age_to'] }}"
                />
          </div>
      </div>
    </div>
    <div class="col-sm-3">
      <label class="col-md-2 col-sm-12 control-label text-white">@lang('Country')</label>
      <div class="form-group">
        <div class="bfh-selectbox bfh-countries bootstrap-select" data-country="{{ $filters['pref_country'] }}" data-flags="true">
          <input type="hidden" name="filters[pref_country]" value="{{ $filters['pref_country'] }}">
          <a class="bfh-selectbox-toggle" role="button" data-toggle="bfh-selectbox" href="#">
            <span class="bfh-selectbox-option input-medium" data-option=""></span>
            <b class="caret"></b>
          </a>
          <div class="bfh-selectbox-options">
            <input type="text" class="bfh-selectbox-filter">
            <div role="listbox">
            <ul role="option">
            </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-sm-2">
      <button class="button btn-lg btn-theme full-rounded animated right-icn" type="submit">
        <span>@lang('search')<i class="glyph-icon flaticon-hearts" aria-hidden="true"></i></span>
      </button>
    </div>
  </div>
{{ Form::close() }}
