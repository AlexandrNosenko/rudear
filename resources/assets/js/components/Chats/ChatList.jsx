import React from 'react';
import PropTypes from 'prop-types';

// import "../styles/styles.scss"

class ChatList extends React.Component {

    constructor(props) {
        super(props);

        this.handleInputChange = this.handleInputChange.bind(this)
        this.decideReceiver = this.decideReceiver.bind(this)
    }

    handleInputChange(e){
        this.setState({[e.target.name]: e.target.value})
    }

    decideReceiver(chat){
        const {currentUser} = this.props;
        return currentUser.id == chat.sender.id ? chat.receiver : chat.sender
    }

    toogleList(){
        $(".chat-left-aside").toggleClass("open-pnl");
        $(".open-panel i").toggleClass("ti-angle-left");
    }

    render(){
        const {selectedChat, chats, currentUser, translations} = this.props;

        return(
            <div className="chat-left-aside">
                <div
                    className="open-panel"
                    onClick={this.toogleList}
                ><i className="ti-angle-right"></i></div>

                <div className="chat-left-inner">
                    <div className="form-material">
                        <input className="form-control p-20" type="text" placeholder={translations['searchContact']} />
                    </div>
                    <ul className="chatonline style-none ">
                        {chats.map((chat)=>
                          <li
                            key={`d-${chat.id}`}
                            className={`nav-item ${selectedChat.id == chat.id ? 'active' : ''}`}
                            onClick={()=>this.props.onChange(chat)}
                            >
                                <img src={this.decideReceiver(chat).avatar} alt="avatar" className="img-circle"/>
                                <p>
                                    {this.decideReceiver(chat).name}
                                    {this.decideReceiver(chat).email}
                                </p>
                                <p>
                                    {this.decideReceiver(chat).is_online ?
                                      <small className="online text-success">{translations['online']}</small>
                                    :
                                      <small className="offline text-danger">{translations['offline']}</small>
                                    }
                                </p>
                          </li>
                        )}
                    </ul>
                </div>
            </div>
        )
    }
}

ChatList.propTypes = {
    //TEMP
    translations: PropTypes.object.isRequired,
    // locale: PropTypes.string.isRequired,
    chats: PropTypes.array.isRequired,
    selectedChat: PropTypes.object.isRequired,
    onChange: PropTypes.func.isRequired
};

export default ChatList;
