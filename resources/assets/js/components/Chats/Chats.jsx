import React from 'react';
import PropTypes from 'prop-types';
import ChatList from './ChatList';
import ChatMessages from './ChatMessages';

class Chats extends React.Component {
    static defaultProps = {
        locale: 'en'
    }

    constructor(props) {
        super(props);


        this.handleActiveChatChange = this.handleActiveChatChange.bind(this)
    }

    componentWillMount(){
        this.setState({ loading: true})

        $.ajax({
            url: '/chats.json',
            success: (chats) => {
                this.setState({
                    chats: chats,
                    activeChat: chats.length > 0 ? chats[0] : {},
                    loading: false
                })
                console.log(chats)
            },
            error: function(data){
              console.log(data);
            }
        })

    }

    handleActiveChatChange(activeChat){
        this.setState({ activeChat });
    }

    render() {
        if(this.state.loading) return <p>loading...</p>
        else return (
            <div className="chat-main-box">
                <ChatList
                    chats={this.state.chats}
                    currentUser={this.props.currentUser}
                    selectedChat={this.state.activeChat}
                    onChange={this.handleActiveChatChange}
                    translations={this.props.translations}
                />
                <ChatMessages
                    currentUser={this.props.currentUser}
                    chat={this.state.activeChat}
                    onChange={this.handleActiveChatChange}
                    translations={this.props.translations}
                  />
            </div>
        );
    }
}

Chats.propTypes = {
    //TEMPORARY SOLUTIONS
    translations: PropTypes.object.isRequired,
    // locale: PropTypes.string.isRequired,
    currentUser: PropTypes.object.isRequired,
};

export default Chats;
