import React from 'react';
import PropTypes from 'prop-types';

// import "../styles/styles.scss"

class ChatMessages extends React.Component {
    static defaultProps = {
        locale: 'en'
    }

    constructor(props) {
        super(props);

        this.state = {
            messageText: '',
            visibleMessagesCount: 10,
            canShowMore: true
        }

        this.handleInputChange = this.handleInputChange.bind(this)
        this.sendMessage = this.sendMessage.bind(this)
        this.isOwnedByCurrentUser = this.isOwnedByCurrentUser.bind(this)
        this.handleMessageSubscribtion = this.handleMessageSubscribtion.bind(this)
        this.rednerChatMessage = this.rednerChatMessage.bind(this)
        this.visibleMessages = this.visibleMessages.bind(this)
        this.showMore = this.showMore.bind(this)

    }

    componentDidMount(){
        $('.chat-list').animate({scrollTop: 200000044});

        this.handleMessageSubscribtion(this.props.chat)
    }

    componentWillReceiveProps(props){
        if( this.props.chat.id != props.chat.id ){
            this.handleMessageSubscribtion(props.chat)
            this.setState({
                visibleMessagesCount: 10,
                canShowMore: true
            })
        }
    }

    handleMessageSubscribtion(activeChat){
        window.EchoInst.channel(`chatroom${this.props.chat.id}`).unsubscribe();

        // EchoInst.private(`chatroom${$('.chat-item:first').data('chat-id')}`)
        window.EchoInst.channel(`chatroom${activeChat.id}`).subscribe()
        window.EchoInst.channel(`chatroom${activeChat.id}`)
            .listen('MessagePosted', (data) => {
                console.log('MessagePosted', data)
                if(data.message.sender.id != currentUser.id)
                    this.handleNewMessage(data.message)
            });

        $('.chat-list').animate({scrollTop: 200000044});
    }

    handleNewMessage(message){
        const newChat = { ...this.props.chat };
        newChat.messages.push(message);
        this.props.onChange(newChat)
        $('.chat-list').animate({scrollTop: 200000044});
    }

    handleInputChange(e){
        this.setState({[e.target.name]: e.target.value})
    }

    sendMessage(){
        const data = this.prepareMessageData();

        if(data.message.text.trim().length != 0){
            //Egerly add new message to conversation
            this.handleNewMessage(data.message)
            this.setState({ messageText: ''})

            $.ajax({
                url: `/messages`,
                type: 'POST',
                data: { message: data },
                success: (message) => {
                  console.log(message)
                },
                error: function(data){
                  console.log(data);
                }
            })
        }
    }

    prepareMessageData() {
        return {
            message: {
                id: new Date(),
                sender_id: currentUser.id,
                sender: currentUser,
                receiver_id: this.decideReceiver().id,
                receiver: this.decideReceiver(),
                text: this.state.messageText,
                created_at: new Date()
            }
        }
    }

    formatDate(created_at){
        return new Date(created_at).getHours() + ":" + new Date(created_at).getMinutes();
    }

    isOwnedByCurrentUser(message){
        return this.props.currentUser.id == message.sender.id
    }

    decideReceiver(){
        const {currentUser, chat} = this.props;
        return currentUser.id == chat.sender.id ? chat.receiver : chat.sender
    }

    rednerChatMessage(message){
        return(
            <li
                key={`m-${message.id}`}
                className={`message-container ${message.sender.id == this.props.currentUser.id ? 'odd' : ''}`}>

                {!this.isOwnedByCurrentUser(message) ?
                  <div className="chat-image">
                    <img alt="Avatar" src={this.decideReceiver().avatar} />
                  </div>
                :null}

                <div className="chat-body">
                    <div className="chat-text">
                        {!this.isOwnedByCurrentUser(message) ?
                          <h4>{this.decideReceiver().name}</h4>
                        :null}
                        <p>{message.text}</p>
                        <b>{this.formatDate(message.created_at)}</b>
                    </div>
                </div>
            </li>
        )
    }

    visibleMessages(){
        const {chat} = this.props;
        const lastIndex = chat.messages.length - 1;

        let messages;

        if(chat){
            messages = chat.messages.slice(lastIndex - this.state.visibleMessagesCount + 1 , lastIndex)
        }

        return messages || [];
    }

    showMore(){
        console.log('asassa')
        let newVisibleMessagesCount = this.state.visibleMessagesCount + 10;

        if(newVisibleMessagesCount > this.props.chat.messages.length){
            newVisibleMessagesCount = this.props.chat.messages.length;
            this.setState({canShowMore: false})
        }else{
            //TODO handle loading more messages from the server
        }

        this.setState({visibleMessagesCount: newVisibleMessagesCount})
    }

    render(){
        const {chat, currentUser, translations} = this.props;

        return(
            <div className="chat-right-aside">
                <div className="chat-main-header">
                    <div className="p-20 b-b">
                        <h3 className="box-title">
                            {translations['chatMessage']}
                        </h3>
                    </div>
                </div>
                <div className="chat-box">
                    <ul className="chat-list chat-messages slimscroll pt-4" style={{overflowY: "scroll"}}>
                        {this.state.canShowMore ?
                            <div className="row show-more">
                                <button
                                    className="btn btn-success"
                                    onClick={this.showMore}
                                >Show more</button>
                            </div>
                        :null}

                        {this.visibleMessages().map((message)=>
                            this.rednerChatMessage(message)
                        )}
                    </ul>

                  <div className="send-chat-box message-input">
                    <textarea
                        name="messageText"
                        id="chat-form-input"
                        className="form-control"
                        placeholder={translations['typeYourMessage']}
                        value={this.state.messageText}
                        onChange={this.handleInputChange}
                        onKeyDown={(e) => e.keyCode == 13 ? this.sendMessage() : null}
                    ></textarea>
                    <div className="custom-send">
                      <a className="cst-icon" data-toggle="tooltip" title="Insert Emojis">
                        <i className="ti-face-smile"></i>
                      </a>
                      <a className="cst-icon" data-toggle="tooltip" title="File Attachment">
                        <i className="fa fa-paperclip"></i>
                      </a>
                      <button
                        id="chat-form-submit"
                        className="btn btn-danger btn-rounded"
                        type="button"
                        onClick={this.sendMessage}
                        >{translations['send']}</button>
                    </div>
                  </div>
              </div>
            </div>
        )
    }
}

ChatMessages.propTypes = {
    //TEMP
    translations: PropTypes.object.isRequired,
    // locale: PropTypes.string.isRequired,
    currentUser: PropTypes.object.isRequired,
    chat: PropTypes.object.isRequired,
    onChange: PropTypes.func.isRequired
};

export default ChatMessages;
