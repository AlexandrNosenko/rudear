$(document).ready(function(){
  // Radio buttons for info page

  $('[name="_gender"]').click(function(){
    $('input[name="gender"]').attr('value', $(this).val());    
  })

  $('[name="_smoker"]').click(function(){
    $('input[name="user_infos[smoker]"]').attr('value', $(this).val());    
  })

  $('[name="_drinker"]').click(function(){
    $('input[name="user_infos[drinker]"]').attr('value', $(this).val());    
  })


  // Radio buttons for preferences page

  $('[name="_pref_gender"]').click(function(){
    $('input[name="user_infos[pref_gender]"]').attr('value', $(this).val());    
  })



  $('#user-info-form .btn-theme').on('click', function(event){
    var min = $('#pre-age-min').val();
    var max = $('#pre-age-max').val();

    $('[name="user_infos[pref_age]"]').val(min + "-" + max);

    $('#user-info-form').submit();
  })
  
  $('#allow-location').click(function(){
    navigator.geolocation.getCurrentPosition(function(data){
      console.log(data);
    });
  })
})