$(document).ready(function(){
  $('.copy-link-btn').on('click', function(){

    var linkToCopy = $( "<input style='position: absolute; left: -100000px;' type='text' class='hidden-link-copy'/>" );
    linkToCopy.val( $('.reference-link').text() + $(this).parent().find('.link-value').val() );

    console.log(linkToCopy.val())

    $('body').append(linkToCopy);

    linkToCopy.select();
    document.execCommand("copy");

    toastr.info("Link Copied!");

  })
})