function prepareAjaxData(data, _token=null){
  var res = {};
  if( !window.csrf_token ) res['_token'] = window.csrf_token;

  data.forEach(function(el) {
    res[el.name] = el.value;
  })

  return res;
}

function handleAjaxHtml(container, data) {
  $(container).append(data.responseText);
}

$(document).ready(function(){
  var page = 1;
  var showMoreBtnContent = $('.show-more-users').html();

  $('.show-more-users').on('click', function(){
    page++;

    var data = prepareAjaxData($('#filters-form').serializeArray());
    data['page'] = page;
    $(this).html("Loading....");

    $.ajax({
       url : $('.all-users-container').data('ajaxurl'),
       method : "GET",
       data : data,
       dataType : "json"
    }).done(function( data ) {
        handleAjaxHtml('.all-users-container', data)
    }).fail(function(data) {
        handleAjaxHtml('.all-users-container', data)
        $('.show-more-users').html(showMoreBtnContent)
    });
  })
})