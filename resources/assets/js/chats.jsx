import React from 'react';
import ReactDOM from 'react-dom';
import Chats from './components/Chats/Chats';

$(document).ready(()=>
    ReactDOM.render(
        <Chats
            currentUser={window.currentUser}
            translations={window.translations}
        />, document.getElementById('chats')))
