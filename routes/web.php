<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/payments', 'PaymentsController@index');
Route::get('/payments/paypalSuccess', 'PaymentsController@paypalSuccess');
Route::get('/payments/paypalCancel', 'PaymentsController@paypalCancel');


Route::group([
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
  ], function(){

    Auth::routes();

    Route::get('/', 'HomeController@index')->name('root');


    Route::get('/profiles', 'ProfilesController@index')->name('profiles');
    // Ajax load more
    Route::get('/profiles.js','ProfilesController@indexAjax' )->name('profiles-ajax');
    Route::get('/profile/{id}', 'ProfilesController@show')->name('profile');


    Route::get('/register/photos', 'Auth\RegisterStepsController@photos')->name('register-photos');
    Route::get('/register/info', 'Auth\RegisterStepsController@info')->name('register-info');
    Route::get('/register/preferences', 'Auth\RegisterStepsController@preferences')->name('register-preferences');


    Route::get('/chats', 'ChatsController@index')->name('chats');
    Route::get('/chats.json', 'ChatsController@indexJson');
    Route::post('/chats', 'ChatsController@create')->name('create-chat');

    /* Favorite and BlackList user managment */
    Route::get('/favorites', 'FavoriteUsersController@index')->name('favorites');
    Route::post('/favorites', 'FavoriteUsersController@create');

    Route::get('/blacklist', 'BlackListController@index')->name('black_list');
    Route::post('/blacklist', 'BlackListController@create');

    Route::get('/likes', 'UserActivityController@index')->name('user_activity');

});

Route::get('/messages.js', 'MessagesController@indexAjax')->name('messages-ajax');
Route::post('/messages', 'MessagesController@create')->name('messages');


// ReferenceCodes
Route::get('/r/{token}', 'ReferenceLinksController@useLink');


// OAuth Routes
Route::get('auth/{provider}', 'Auth\AuthController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\AuthController@handleProviderCallback');

Route::get('loginWithFacebook', 'Auth\AuthController@loginWithFacebook');

// Vip status managment
Route::get('/vip-statuses/trial', 'VipStatusesController@trial')->name('free-trial');


Route::post('/register/homepage', 'Auth\RegisterStepsController@homepage')->name('homepage-register');


Route::post('/photos', 'PhotosController@create')->name('photos');
Route::delete('/photo/{id}', 'PhotosController@delete')->name('photo');
Route::patch('/photo/{id}', 'PhotosController@update');


Route::patch('/profile/update', 'ProfilesController@update')->name('profile-update');
Route::patch('/profile/updateInfo', 'ProfilesController@updateInfo')->name('profile-update-info');
Route::patch('/profile/updatePref', 'ProfilesController@updatePref')->name('profile-update-pref');


Route::get('/references/new', [ 'middleware' => ['auth'], 'uses' => 'ReferenceLinksController@new'] )->name('reference-links-new');
Route::post('/references', [ 'middleware' => ['auth'], 'uses' => 'ReferenceLinksController@create'] )->name('reference-links');
