let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.webpackConfig({
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['react', 'stage-0']
          }
        }
      }
    ]
  }
})

// TODO MIX()
mix.react('resources/assets/js/chats.jsx', 'public/js');

mix.js('resources/assets/js/app.js', 'public/js')
  .js('resources/assets/js/info.js', 'public/js')
  .js('resources/assets/js/profiles.js', 'public/js')
  .js('resources/assets/js/references.js', 'public/js')
  .sass('resources/assets/sass/app.scss', 'public/css')
  .sass('resources/assets/sass/profiles.scss', 'public/css')
  .sass('resources/assets/sass/info.scss', 'public/css')
  .sass('resources/assets/sass/chats.scss', 'public/css')
  .sass('resources/assets/sass/home.scss', 'public/css')
  .sass('resources/assets/sass/favorites.scss', 'public/css');

