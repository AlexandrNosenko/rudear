$(document).ready(function(){
  $('.upload-btn').on('click', function(){
    $('#user-images-form').submit();
  })

  $('.favorite-toggle').on('click', function(){
    $(this).closest('form').submit();
  })
  
  

  $('.image-placeholder').on('click', function(){
    $('[name="photos"]').click();
    $('[name="photos"]').change(function(e) {
      if(e.target.files.length > 0 )
        $('#user-images-form').submit();
    });
  })

  $('.image-upload-btn').on('click', function(){
    $('[name="photos"]').click();
    $('[name="photos"]').change(function(e) {
      var filename = " | " + e.target.files[0].name;
      $('.file-name').text(filename);
      $('.upload-btn').css('display', 'inline-block');
    });
  })

})
